/*
 * Copyright 2015 Fibar Group S.A.
 * All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Fibar Group S.A. and its suppliers,
 * if any. The intellectual and technical concepts contained
 * herein are proprietary to Fibar Group S.A. and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Fibar Group S.A.
 */
#include <future>
#include <iostream>
#include <thread>
#include "Poco/NamedEvent.h"

int main(int argc, char* argv[]) {
  if (argc != 2 && argc != 3) {
    std::cout << "Usage: wait_for_event EVENT_NAME [TIMEOUT_S]" << std::endl;
    return 1;
  }

  if (argc == 2) {
    Poco::NamedEvent event(argv[1]);
    event.wait();
  } else {
    std::future<void> future = std::async(std::launch::async, [&]() {
      Poco::NamedEvent event(argv[1]);
      event.wait();
    });

    auto timeout = std::chrono::seconds{atoi(argv[2])};

    if (future.wait_for(timeout) != std::future_status::ready) {
      exit(2);
    }
  }

  return 0;
}
