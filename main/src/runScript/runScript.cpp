#include <iostream>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    std::string cmd = "";

    for (int i = 1; i < argc; i++)
    {
        cmd.append("\"");
        cmd.append(argv[i]);
        cmd.append("\" ");
    }

    system(cmd.c_str());

    return 0;
}
