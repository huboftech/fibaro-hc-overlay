#!/bin/sh

#include config
. /opt/fibaro/scripts/config
. /opt/fibaro/system/status-utils
. /opt/fibaro/scripts/logging.sh
. /opt/fibaro/scripts/utils/errorCodeMessages.sh

setup_self_logging "fibaro.scripts.$(basename "${0}")" "$@"

_downloadBackup()
{
  [ $# -ne 1 ] && { echo "internal error: usage downloadBackup <backup-id>"; _endScript 1; }
  local backupId="$1"

  echo "Downloading backup from ${remoteBackupServer}..."
  set_status_backup_restore_downloading_backup
  safe_remove_dir "${tmpBackupPath}"
  _checkAndCreateDir "${tmpBackupPath}"
  touch "${tmpBackupPath}"/"${backupFilename}"

  url="${remoteGetBackupScript}?PK_AccessPoint=$(getSerial)&HW_Key=$(getHWKey)&backupID=${backupId}"
  httpCode=$(curl -f -s -w "%{http_code}" --retry 3 -X GET -o "${tmpBackupPath}/${backupFilename}" "${url}")
  curlCode=${?}

  if [ "${httpCode}" -eq 0 ] && [ "${curlCode}" -ne 0 ] ; then
    echo "$(_error_code_msg curl ${curlCode}) while downloading backup"
    set_status_backup_restore_connection_error
    return 1
  elif [ "${httpCode}" -lt 200 ] || [ "${httpCode}" -ge 300 ] ; then
    echo "$(_error_code_msg http "${httpCode}") while downloading backup"

    if [ "${httpCode}" -eq 404 ] ; then
      echo "connection error"
      set_status_backup_restore_error_remote_not_found
    else
      set_status_backup_restore_connection_error
    fi
    return 1
  fi
}

_copyLocalBackup()
{
  fileName=$(echo "${backupPath}"/*.fbi* | sed "s/.*\\///")
  echo "Copy backup from ${backupPath}/${fileName} to ${tmpBackupPath}..."

  if [ ! -f "${backupPath}/${fileName}" ]; then
    echo "Local backup not exist"
    set_status_backup_restore_error_local_not_found
    return 1
  fi

  safe_remove_dir "${tmpBackupPath}"
  _checkAndCreateDir "${tmpBackupPath}"

  7zr x "${backupPath}/${fileName}" -o"${tmpBackupPath}" "${backupFilename}"
}

_extractBackup()
{
  set_status_backup_restore_extracting_backup

  if [ ! -f "${tmpBackupPath}/${backupFilename}" ]; then
    echo "Backup not exist in ${tmpBackupPath}"
    set_status_backup_restore_error_extracting_backup
    return 1
  fi

  7zr x "${tmpBackupPath}/${backupFilename}" -o"${tmpBackupPath}"

  rm -f "${tmpBackupPath}/${backupFilename}"

  if [ ! -f "${tmpBackupPath}/db" ] || [ ! -f "${tmpBackupPath}/${zwaveFile}" ]; then
    echo "Backup extracting error"
    set_status_backup_restore_error_extracting_backup
    return 1
  fi
}

_writeZwaveImage()
{
  echo "Writing Z-Wave image..."
  set_status_backup_restore_restoring_zwave_dump
  if ! writeZwaveEeprom "${tmpBackupPath}/${zwaveFile}"; then
    set_status_backup_restore_error_restoring_zwave_dump
    sleep 5
    _startServices
    _endScript 1
  fi
}

_restoreUserData()
{
  echo "Restoring user_data..."
  set_status_backup_restore_restoring_user_data
  if ! restoreUserData ; then
    set_status_backup_restore_error_restoring_user_data
    sleep 5
    _startServices
    _endScript 1
  fi
  set_status_backup_restore_cleaning_finished
}

_updateSoftware()
{
  [ $# -ne 1 ] && { echo "internal error: usage update_softwate <version>"; _endScript 1; }
  local version="${1}"
  ${updateScript} --auto "${version}" --without-backup
}

_convertData()
{
  "${updateDataScript}" start
  _startServices
}

_restore()
{
  local backupType="${1}"
  local backupId="${2}"
  local downloadVersion="${3}"  # this param may be empty

  case "${backupType}" in
    "--local")
      if ! _copyLocalBackup; then
        _endScript 1
      fi
      ;;

    "--remote")
      if ! _downloadBackup "${backupId}"; then
        _endScript 1
      fi
      ;;

    *)
      echo "Invalid backup type: ${backupType}"
      _endScript 1
      ;;
  esac

  set_status_backup_restore_stop_service
  _stopServices

  if ! _decryptBackup; then
    sleep 5
    _startServices
    _endScript 1
  fi

  if ! _extractBackup; then
    sleep 5
    _startServices
    _endScript 1
  fi

  _writeZwaveImage
  _restoreUserData

  if [ -n "${downloadVersion}" ]; then
    _updateSoftware "${downloadVersion}"
  else
    _convertData
  fi

  _endScript 0
}

# main

if [ ${#} -lt 2 ] || [ ${#} -gt 3 ]; then
  _error "invalid number of arguments"
  echo "usage: ${0} <backup-type> <backup-id> [update-version]";
  exit 1
fi

echo "Restoring backup..."
led_recovery blink_slow
_restore "${1}" "${2}" "${3}"
