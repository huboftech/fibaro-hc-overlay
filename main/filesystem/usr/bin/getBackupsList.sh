#!/bin/sh
. /opt/fibaro/scripts/config

getValue()
{
  if [ -f "${1}" ]; then
    sed -n "s/^${2}=*//p" "${1}"
  else
    echo ""
  fi
}

convertSize()
{
  divider=1000000
  if [ "${1}" -gt "${divider}" ]; then
    number=$(dc "${1}" "${divider}" / p)
    number=$(printf "%.1f" "${number}")
    echo "${number}M"
    exit
  fi

  divider=1000
  if [ "${1}" -gt "${divider}" ]; then
    number=$(dc "${1}" "${divider}" / p)
    number=$(printf "%.1f" "${number}")
    echo "${number}K"
    exit
  fi

  echo "${1}"
}

getLocalBackupsList()
{
  webPath="/var/www";
  filesystemPath="/mnt/download/backups";

  fileName=$(echo "${filesystemPath}"/*.fbi* | sed "s/.*\\///")

  if [ ! -n "${filename}" ]; then
    filename="backup.fbi"
  fi

  if [ -f "${filesystemPath}/${fileName}" ] && [ -f "${filesystemPath}/info" ]; then
    apiFilePath="/backups/${fileName}"

    timestamp=$(getValue "${filesystemPath}/info" "timestamp")
    devices=$(getValue "${filesystemPath}/info" "devices")
    rooms=$(getValue "${filesystemPath}/info" "rooms")
    scenes=$(getValue "${filesystemPath}/info" "scenes")
    description=$(getValue "${filesystemPath}/info" "description")
    softVersion=$(getValue "${filesystemPath}/info" "version")
    size=$(getValue "${filesystemPath}/info" "size")
    size=$(convertSize "${size}")

    result="{\"type\":\"local\",\"localBackupPath\":\"${apiFilePath}\",\"uploadedBackup\":[{\"id\":\"1\",\"timestamp\":${timestamp},\"compatible\":true,\"devices\":${devices},\"rooms\":${rooms},\"scenes\":${scenes},\"description\":\"${description}\",\"softVersion\":\"${softVersion}\",\"filename\":\"${fileName}\",\"size\":\"${size}\"}]}"

    echo "${result}"
  else
    echo "{\"type\":\"local\",\"localBackupPath\":\"\",\"uploadedBackup\":[]}"
  fi
}

getRemoteBackupsList()
{
    serial=$(getSerial)
    hwkey=$(getHWKey)

    file="/tmp/getBackupsList.log"

    url="${remoteGetBackupsListScript}?PK_AccessPoint=${serial}&HW_Key=${hwkey}"
    code=$(curl -f -s -w "%{http_code}" -o "${file}" -X GET "${url}")

    if [ "${code}" -ge 200 ] && [ "${code}" -lt 300 ] ; then
      cat "${file}"
    fi

    rm "${file}"
}

if [ ${#} -lt 1 ] ; then
    exit 1
fi

type="${1}"

case "${type}" in
  "--local")
    getLocalBackupsList
    ;;

  "--remote")
    getRemoteBackupsList
    ;;

  *)
    exit 2
    ;;
esac

exit 0
