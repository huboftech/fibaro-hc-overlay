#!/bin/sh
. /opt/fibaro/scripts/config

#TODO extract to other file
#logging functions

# colors
RED='\033[1;31m'
GREEN='\033[1;32m'
BLUE='\033[0;34m'
YELLOW='\033[0;33m'
NORMAL='\033[0m'
dbPath=${configDbPath}

log()
{
	# date=$(date +'%b %e %R ')
	echo "$1 $NORMAL"
}

logError()
{
	log "$RED$1"
}

logInfo()
{
	log "$BLUE$1"
}

logWarning()
{
 	log "$YELLOW$1"
}

logSuccess()
{
	log "$GREEN$1"
}

clearOldLogs()
{
  logInfo "Removing old logs"
  rm /tmp/"${dumpNameBaseName}"*
}

ENCODE_ARCHIVE=false
CLEAR_LOGS=false

for i in "$@"
do
    case $i in
        -e|--encode) # encode output file. Prevent user from checking logs.
        ENCODE_ARCHIVE=true
        shift
        ;;
        -c|--clear) # remove logs file from /tmp. Mainly for usage from logsDump.php file
        CLEAR_LOGS=true
        shift
        ;;
    esac
done

dumpNameBaseName="LogsDump" 

if $CLEAR_LOGS; then
    clearOldLogs
    exit 0
fi

SYSTEM_LOGS_DIRECTORY='systemLogs'
TMP_DIRECTORY="/tmp/$SYSTEM_LOGS_DIRECTORY"

if [ ! -d ${TMP_DIRECTORY} ]; then
    mkdir -p ${TMP_DIRECTORY};
else
	rm -f ${TMP_DIRECTORY}/*
fi;

cd ${TMP_DIRECTORY} || exit 1

############### user.log ###########################
logInfo "Dumping user info into user.log"

w -u > user.log

echo -n "Dump was done by: " >> user.log
whoami >> user.log


############### dmesg.log ##########################
logInfo "Dumping dmesg into dmesg.log"


dmesg > dmesg.log

############### ps.log #############################
logInfo "Dumping process into ps.log"


ps aux > ps.log

###############netstat.log #########################
logInfo "Dumping netstat"

netstat -a > netstat.log

############### disk.log ###########################
logInfo "Dumping disk info into disk.log "

df -h > disk.log

############### meminfo.log ########################
logInfo "Dumping meminfo into meminfo.log "

cat /proc/meminfo > meminfo.log

############### cpu.log ############################
logInfo "Dumping cpu info into cpu.log "

ps -eo pcpu,pid,user,args | sort -k 1 -r > cpu.log

############### version.log #########################
logInfo "Dumping version number from HC_DATA table into version.log"

cat /etc/build-version > version.log
sqlite3 /opt/fibaro/db 'SELECT version FROM HC_DATA' >> version.log

############### serial_number.log ########################
logInfo "Dumping serial numer from GPIOServer int serlia_number.log"

getSerial > serial_number.log
############### db_size.log ########################
logInfo "Dumping database size into db_size"

ls -allh "${dbPath}" > db_size.log

############### http_req.log ####################
logInfo "Dumping some results of http requests into http_req"

{ echo "127.0.0.1:11111/api/loginStatus:" >> req_to_serv.log; curl 127.0.0.1:11111/api/loginStatus; } >> req_to_serv.log 2>&1
{ echo -e "\\n127.0.0.1/api/loginStatus:" >> req_to_serv.log; curl 127.0.0.1/api/loginStatus; } >> req_to_serv.log 2>&1

############### interface-data.log #################
logInfo "Dumping /api/interface/data into interface-data.log"

curl 127.0.0.1:11111/api/interface/data > interface-data.log  2>/dev/null
############### create tar.gz #################
logInfo "Creating patch with logs ($dbPath)"

cd ../ || exit 0
DATE=$(date +"%d-%m-%Y_%H_%M_%S")
dumpName=$dumpNameBaseName"_${DATE}.tar.gz"

clearOldLogs
tar -czf /tmp/"${dumpName}" hcserver.log* Zwave.log* serviceUpdate.log* "${SYSTEM_LOGS_DIRECTORY}"/*

if $ENCODE_ARCHIVE; then
  openssl enc -aes-256-cbc -in /tmp/"${dumpName}" -out /tmp/"${dumpName}".enc -k fibaro-hc2
  mv /tmp/"{$dumpName}".enc /tmp/"${dumpName}"
fi

if [ ! -e "/tmp/${dumpName}" ]; then
    logError "Create tar.gz file failed. You can find dump files in: ${TMP_DIRECTORY}"
else
	rm -fr "${TMP_DIRECTORY}"
	ln -fs "/tmp/${dumpName}" "/tmp/${dumpNameBaseName}"
	logSuccess "You can find dump file: ${dumpName}"
fi;
