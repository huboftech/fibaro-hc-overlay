require('net.TCPSocket')

sock = net.TCPSocket({timeout=20 * 1000})

function fromhex(str)
    return (str:gsub('..', function(cc)
        return string.char(tonumber(cc, 16))
    end))
end

function tohex(str)
    return (str:gsub('.', function(c)
        return string.format('%02X', string.byte(c))
    end))
end

function parseData(str)
    while true do 
        if string.find(str, '0x') then
            i,j = string.find(str, '0x')
            str = string.sub(str, 1, i - 1) .. fromhex(string.sub(str, i + 2, j +2)) .. string.sub(str, j + 3)
        else
            return str       
        end
    end
end

function send(str)
	strToSend = parseData(str)
	sock:write(strToSend, {
    	success = function()
            waitForResponseFunction()
        end,
        error = function(err)
            close()
        end
    })
end

function close()
    sock:close()
end

function connect(address, port, str)
    sock:connect(address, port, {
        success = function()
            send(str)
        end,
        error = function(err)
            close()
        end,
    })
end

function waitForResponseFunction()
    if waitForResponse then
        sock:read({
            success = function() 
                close() 
            end,
            error = function() 
                close() 
            end
        })
    else
        close()
    end
end

connect(address, port, data)


