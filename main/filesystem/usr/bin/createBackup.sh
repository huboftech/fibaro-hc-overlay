#!/bin/sh

#include config
. /opt/fibaro/scripts/config
. /opt/fibaro/system/status-utils
. /opt/fibaro/scripts/logging.sh
. /opt/fibaro/scripts/utils/errorCodeMessages.sh

setup_self_logging "fibaro.scripts.$(basename "${0}")" "$@"

echo "Creating backup..."

#backup types
backupTypeRemote="--remote"
backupTypeLocal="--local"

_init()
{
    echo "Initializing backup creation..."

    if [ "${1}" != "${backupTypeRemote}" ] && [ "${1}" != "${backupTypeLocal}" ] ; then
        echo "Invalid backup type"
        _endScript 1
    fi

    backupType="${1}"

    description=""
    if [ -n "${2}" ]; then
	    description="${2}"
    fi

    autoBackup="false"
    if [ "${3}" ==  "--autobackup" ]; then
	    autoBackup="true"
    fi

    serialNumber=$(getSerial)
    HWKey=$(getHWKey)
    version=$(getVersion)

    devices=$(sqlQuery "SELECT count(*) FROM NEW_Device WHERE Device_Type='com.fibaro.zwaveDevice'")
    rooms=$(sqlQuery "SELECT count(*) FROM Room")
    scenes=$(sqlQuery "SELECT count(*) FROM NEW_Scene")

    set_status_backup_create_stop_service
    _stopServices
}

_createInfoFile()
{
    timestamp=$(date +%s)

    year=$(date -d @"${timestamp}" +%Y)
    month=$(date -d @"${timestamp}" +%m)
    day=$(date -d @"${timestamp}" +%d)
    hour=$(date -d @"${timestamp}" +%H)
    minute=$(date -d @"${timestamp}" +%M)
    second=$(date -d @"${timestamp}" +%S)

    echo "Creating info file..."
    _checkAndCreateDir "${tmpBackupPath}"
    infoFile="${tmpBackupPath}/info"

    sizeInBytes()
    {
        du -b "${1}" | awk '{print $1}'
    }

    #size only includes size of backup file; $infoFile size not included
    hcSerial=$(getSerial)
    size=$(sizeInBytes "${tmpBackupPath}/${backupFilename}")

    (
        echo "hcSerial=${hcSerial}"
        echo "devices=${devices}"
        echo "rooms=${rooms}"
        echo "scenes=${scenes}"
        echo "year=${year}"
        echo "month=${month}"
        echo "day=${day}"
        echo "hour=${hour}"
        echo "minute=${minute}"
        echo "second=${second}"
        echo "timestamp=${timestamp}"
        echo "description=${description}"
        echo "version=${version}"
        echo "size=${size}"
    ) > "${infoFile}"
}

_prepareFbiFile()
{
    set_status_backup_create_user_data

    dateTime=$(date +%Y_%m_%d-%H_%M_%S)
    hcSerialNumber=$(getSerial)
    fbiFileName="backup_${hcSerialNumber}_${dateTime}"

    _checkAndCreateDir "${tmpBackupPath}"
    cd "${tmpBackupPath}" || exit

    rm -rf ./*.fbi
    tar -cf - "./${backupFilename}" "./info" | 7zr a -m0=lzma2 -mx "${tmpBackupPath}/${fbiFileName}.7z"
    mv "${tmpBackupPath}/${fbiFileName}.7z" "${tmpBackupPath}/${fbiFileName}.fbi"
    rm -rf "${tmpBackupPath:?}/${backupFilename:?}"
}

_upload()
{
    rm -f "${backupPath}/testBackup.log"
    touch "${backupPath}/testBackup.log"

    echo "Uploading backup to ${remoteBackupServer}..."
    set_status_backup_create_upload_to_server

    httpCode=$(curl -f -s -w "%{http_code}" --retry 3 -F "PK_AccessPoint=${serialNumber}" -F "HW_Key=${HWKey}" -F "version=${version}" -F "description=${description}" -F "devices=${devices}" -F "rooms=${rooms}" -F "scenes=${scenes}" -F "backup=@${tmpBackupPath}/${backupFilename}" -F "automatic=${autoBackup}" -o "${backupPath}/testBackup.log" "${remoteSaveBackupScript}")
    curlCode=${?}

    if [ "${httpCode}" -eq 0 ] && [ "${curlCode}" -ne 0 ] ; then
        echo "$(_error_code_msg curl ${curlCode}) while uploading backup"
        set_status_backup_create_error_upload_backup_to_server
        sleep 5
        _startServicesIfManualBackup
        _endScript 5
    elif [ "${httpCode}" -lt 200 ] || [ "${httpCode}" -ge 300 ] ; then
        echo "$(_error_code_msg http "${httpCode}") while uploading backup"

        if [ "${httpCode}" -eq 507 ] ; then
          set_status_backup_create_error_not_enough_space
        else
          set_status_backup_create_error_upload_backup_to_server
        fi

        sleep 5
        _startServicesIfManualBackup
        _endScript 6
    else
        echo "uploaded"
    fi
}

_finalize()
{
    sleep 2
    _startServicesIfManualBackup

    sync
    _endScript 0
}

_finalizeLocal()
{
    fileName=$(echo "${tmpBackupPath}"/*.fbi* | sed "s/.*\\///")

    if [ -f "${tmpBackupPath}/${fileName}" ] && [ -f "${tmpBackupPath}/info" ]; then
        safe_clean_dir "${backupPath}"

        mv "${tmpBackupPath}/${fileName}" "${backupPath}"
        mv "${tmpBackupPath}/info" "${backupPath}"
    fi
}

_finalizeRemote()
{
    fileName=$(echo "${tmpBackupPath}"/*.7z* | sed "s/.*\\///")

    if [ -f "${tmpBackupPath}/${fileName}" ]; then
        rm -rf "${tmpBackupPath:?}/${fileName:?}"
    fi
}

_initialSettings

_init "${1}" "${2}" "${3}"
initializationSucceeded=${?}

if [ "${initializationSucceeded}" -eq 0 ]; then
    _createBackupFile
    _encryptBackup
else
    echo "Initialization failed"
    _endScript 1
fi

if [ "${backupType}" == "${backupTypeLocal}" ] ; then
    _createInfoFile
    _prepareFbiFile
    _finalizeLocal
elif [ "${backupType}" == "${backupTypeRemote}" ] ; then
    _upload
    _finalizeRemote
fi

_finalize
