#!/bin/sh

#include config
. /opt/fibaro/scripts/config
. /opt/fibaro/scripts/logging.sh

setup_self_logging "fibaro.scripts.$(basename "${0}")" "$@"

_deleteLocalBackup()
{
    echo "Removing local backup ..."

    fileName=$(echo "${backupPath}"/*.fbi* | sed "s/.*\\///")

    if [ -f "${backupPath}/${fileName}" ]; then
        rm -rf "${backupPath:?}/"*
        echo "Backup has been successfully removed"
    else
        echo "Backup has not been found on filesystem"
        exit 3
    fi
}

_deleteRemoteBackup()
{
    local backupID; backupID=${1}

    if [ -n "${backupID}" ]; then
        echo "Removing remote backup ${backupID}..."

        url="${remoteDeleteBackupScript}?PK_AccessPoint=$(getSerial)&HW_Key=$(getHWKey)&backupID=${backupID}"
        code=$(curl -f -s -w "%{http_code}" --retry 1 -X DELETE "${url}")

        if [ "${code}" -ge 200 ] && [ "${code}" -lt 300 ] ; then
            echo "Backup ${backupID} has been successfully removed with code ${code}"
        else
            _error "${remoteDeleteBackupScript} returned code ${code}"
            exit 3
        fi
    else
        _error "Invalid backupId: ${backupID}"
        exit 2
    fi
}

if [ ${#} -lt 2 ]; then
    _error "not enough arguments"
    exit 1
fi

backupType="${1}"

case ${backupType} in
    "--local")
        _deleteLocalBackup
        ;;

    "--pendrive")
        _deletePendriveBackup "${2}"
        ;;

    "--remote")
        _deleteRemoteBackup "${2}"
        ;;

    *)
        _error "Invalid backup type ${backupType}"
        ;;
esac

exit 0
