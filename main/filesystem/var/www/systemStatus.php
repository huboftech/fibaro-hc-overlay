<?php
  require("/var/www/localization.php");

  function readFileAndTrim($filePath)
  {
    $fileHandle = fopen($filePath, "r") or die("");
    $fileContent = fread($fileHandle, filesize($filePath));
    $fileContent = trim($fileContent);

    fclose($fileHandle);
    return $fileContent;
  }

  function parseStatusFile()
  {
    $SYSTEM_STATUS_FILE_PATH="/opt/fibaro/system/status";

    $status = readFileAndTrim($SYSTEM_STATUS_FILE_PATH);
    
    global $lang;

    $ret = array();
    $ret["status"] = $status;
    $ret["description"] = getMsg($lang, $status);
    $ret["title"] = getMsg($lang, $status . "_TITLE");
    
    return $ret;
  }

  function parseStageFile()
  {
    $SYSTEM_STAGE_FILE_PATH="/opt/fibaro/system/stage";
    if(!file_exists($SYSTEM_STAGE_FILE_PATH))
    {
      return NULL;
    }

    $stage = array();

    $stageContent = readFileAndTrim($SYSTEM_STAGE_FILE_PATH);
    $rows = explode("\n", $stageContent);

    if (count($rows) < 2)
       return $stage;
    
    foreach($rows as $row => $line)
    {
      $explode = explode("=", $line);
      
      if ($explode[0] == "description")
      {
        global $lang;
        $stage[$explode[0]] = getMsg($lang, json_decode(trim($explode[1])));
      }
      else
        $stage[$explode[0]] = json_decode(trim($explode[1]));
    }

    return $stage;
  }
