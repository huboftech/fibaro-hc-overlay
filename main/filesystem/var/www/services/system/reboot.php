<?php
require_once("../../authorize.php");
require("httpStatuses.php");

function tryDecodeJson($str)
{
    $data = json_decode($str);
    return json_last_error() == JSON_ERROR_NONE ? $data : null;
}

function authorize() 
{
    return isAuthorized() || isAuthorizedFibaroAuth(array(role::USER, role::INSTALLER));
}

function handlePOST($text)
{
    if (!isLocalRequest() && !authorize())
    {
       sendUnauthorized();
       return;
    }

    $params = tryDecodeJson($text);
    if(!is_null($params) && isset($params->recovery) && $params->recovery === true)
        exec("rebootToRecovery");
    else
        exec("systemReboot");
}

$requestBody = file_get_contents('php://input');
$requestMethod = $_SERVER['REQUEST_METHOD'];

if ($requestMethod == "POST") 
    handlePOST($requestBody);
else 
    setStatusMethodNotAllowed();
