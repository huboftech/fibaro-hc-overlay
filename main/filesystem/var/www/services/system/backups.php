<?php

require_once("../../authorize.php");
require_once("../../dbSession.php");
require_once("../../instances.php");

function authorize()
{
   if (!isAuthorized() && !isAuthorizedFibaroAuth(array(role::USER, role::INSTALLER)))
   {
      sendUnauthorized();
      exit(0);
   }
}

require("httpStatuses.php");

function printAsJson($text)
{
    echo(json_encode($text));
}

function isJson($text)
{
    json_decode($text);
    return (json_last_error() == JSON_ERROR_NONE);
}

function createBackup($params)
{
    if (getNumberOfInstances('{screen} SCREEN -dmS BACKUP') > 0)
    {
        setStatusTooManyRequests();
        return;
    }

    $type = $params->type;
    $description = urldecode($params->description);

    if (strpos($description, '"') !== false)
    {
        $description = str_replace('"', '‘', $description);
    }

    $hcVersion = exec("cat /mnt/hw_data/serial | cut -c1-3");

    if ($type == "local" && $hcVersion == "HC2") 
    {
        exec('screen -dmS BACKUP /usr/bin/createBackup.sh --local "' . $description . '"');
    }
    elseif ($type == "remote") 
    {
        exec('screen -dmS BACKUP /usr/bin/createBackup.sh --remote "' . $description . '"');
    }
    else 
    {
        setStatusBadRequest();
        return;
    }

    setStatusAccepted();
}

function restoreBackup($params)
{
    if (getNumberOfInstances('{screen} SCREEN -dmS RESTORE') > 0)
    {
        setStatusTooManyRequests();
        return;
    }

    $type = $params->type;
    $id = $params->id;
    $version = $params->version;

    if (is_null($id) || !is_numeric($id) || $id < 1 ) 
    {
        setStatusBadRequest();
        return;
    }

    $hcVersion = exec("cat /mnt/hw_data/serial | cut -c1-3");

    if ($type == "local" && $hcVersion == "HC2" || $type == "remote") 
    {
        $version ?
                exec('screen -dmS RESTORE restoreBackup.sh --' . $type. ' '. $id . ' ' . $version) :
                exec('screen -dmS RESTORE restoreBackup.sh --' . $type. ' '. $id);
    }
    else 
    {
        setStatusBadRequest();
        return;
    }

    setStatusAccepted();
}

function removeOldBackup($id)
{
    $hwkey = exec("cat /mnt/hw_data/hwkey");
    $serial = exec("cat /mnt/hw_data/serial");

    $crl = curl_init();
    curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($crl, CURLOPT_TIMEOUT, 15);
    curl_setopt($crl, CURLOPT_URL, "https://backupshcl.fibaro.com/deleteBackup.php");
    curl_setopt($crl, CURLOPT_POST, 1);
    curl_setopt($crl, CURLOPT_POSTFIELDS, "PK_AccessPoint=" . $serial . "&HW_Key=" . $hwkey . "&backupID=" . $id);

    curl_exec($crl);
}

function removeBackup($type, $id)
{
    $hcVersion = exec("cat /mnt/hw_data/serial | cut -c1-3");

    if ($type == "local" && $hcVersion == "HC2") 
    {
        exec('deleteBackup.sh --local ' . 1, $rmout, $rmstat);

        if ($rmstat) 
        {
            setStatusNotFound();
            return;
        }
    }
    elseif ($type == "pendrive" && $hcVersion == "HC2") 
    {
        if ($id > 0) 
        {
            exec('deleteBackup.sh --pendrive ' . $id, $rmout, $rmstat);
        }
        else 
        {
            setStatusNotFound();
            return;
        }

        if ($rmstat) 
        {
            setStatusNotFound();
            return;
        }
    }
    elseif ($type == "remote") 
    {
        if ($id > 0) 
        {
            exec('deleteBackup.sh --remote ' . $id, $rmout, $rmstat);
        }
        else 
        {
            setStatusNotFound();
            return;
        }

        if ($rmstat) 
        {
            setStatusNotFound();
            return;
        }
    }
    elseif ($type == "remoteOld")
    {
        if ($id > 0) 
        {
            removeOldBackup($id);
        }
        else 
        {
            setStatusNotFound();
            return;
        }
    }
    else {
        setStatusBadRequest();
        return;
    }
}

function searchAfter ($begin, $str)
{
    if (!is_bool(strpos($str, $begin)))
    return substr($str, strpos($str,$begin)+strlen($begin));
}

function searchBefore ($begin, $str)
{
    return substr($str, 0, strpos($str, $begin));
}

function searchBetween ($begin, $end, $str)
{
    return searchBefore ($end, searchAfter($begin, $str));
}

function handleGET()
{
    header('Content-Type: application/json');

    $backups = array();

    $hcVersion = exec("cat /mnt/hw_data/serial | cut -c1-3");

    if ($hcVersion == "HC2")
    {
        require_once("/opt/fibaro/scripts/libs/utils.php");

        $localBackups = exec('getBackupsList.sh --local');
        if (isJson($localBackups) && !empty($localBackups))
        {
            array_push($backups, json_decode($localBackups));
        }

        $pendriveBackups = getPendriveBackupList();
        if (isJson($pendriveBackups) && !empty($pendriveBackups))
        {
            array_push($backups, json_decode($pendriveBackups));
        }
    }

    $remoteBackups = exec('getBackupsList.sh --remote');
    if (isJson($remoteBackups) && !empty($remoteBackups))
    {
        array_push($backups, json_decode($remoteBackups));
    }

    if ($hcVersion == "HCL")
    {
        $serial = exec("cat /mnt/hw_data/serial");
        $hwkey = exec("cat /mnt/hw_data/hwkey");
        $version =  exec("cat /etc/version");

        $crl = curl_init();
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($crl, CURLOPT_TIMEOUT, 15);
        curl_setopt($crl, CURLOPT_URL, "https://backupshcl.fibaro.com/getBackupsList.php");
        curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POSTFIELDS, "PK_AccessPoint=" . $serial . "&HW_Key=" . $hwkey);
        curl_setopt($crl, CURLOPT_HTTPHEADER, array("X_FIBARO_SOFT_VERSION: $version"));

        $result = curl_exec($crl);

        $backupList = json_decode($result);
        $remoteOldBackups = array();

        foreach ($backupList as &$backup) 
        {
            $backupEntry = array(
                'id' => $backup->id,
                'timestamp' => intval($backup->timestamp),
                'compatible' => false,
                'devices' => intval($backup->devices),
                'rooms' => intval($backup->rooms),
                'scenes' => intval($backup->scenes),
                'description' => $backup->description,
                'softVersion' => $backup->softVersion
            );

            array_push($remoteOldBackups, $backupEntry);
        }

        $remoteOldBackups = array(
            'type' => "remoteOld",
            'backups' => $remoteOldBackups
        );

        $remoteOldBackups = json_encode($remoteOldBackups);
        if (isJson($remoteOldBackups) && !empty($remoteOldBackups))
        {
            array_push($backups, json_decode($remoteOldBackups));
        }
    }

    echo !empty($backups) ? json_encode($backups) : "[]";

    setStatusOK();
}

function handlePOST($requestBody) 
{
    authorize();

    if ($_SERVER["CONTENT_TYPE"] == "application/json")
    {
        if (isJson($requestBody))
        {
            $data = json_decode($requestBody);
            $action = $data->action;
            $params = $data->params;

            if ($action == "create") 
            {
                createBackup($params);
            }
            elseif ($action == "restore") 
            {
                restoreBackup($params);
            }
            else 
            {
                setStatusBadRequest();
            }
        }
        else
        {
            setStatusBadRequest();
        }
    }
    else if ($_SERVER["CONTENT_TYPE"] == "application/x-www-form-urlencoded")
    {
        $maxBackupFileSize = 0;

        $hcVersion=exec('cat /mnt/hw_data/serial | cut -c1-3');
        if ($hcVersion == "HC2")
        {
            // FIXME: for HC2 maxBackupFileSize should be 300 MiB
            // Use 150 MiB, becouse with maxBackupFileSize >= 157 MB was problem 
            // (error code: "500" and next "200", probably not enough disk space)   
            $maxBackupFileSize = (int) 150 * 1000 * 1000; // 150 MiB
        }
        else
        {
            setStatusBadRequest();
            return;
        }

        $fileName = $_SERVER['HTTP_FILENAME'];

        if (searchAfter(".", $fileName) != "fbi")
        {
            $reason = array('reason' => "Invalid File Extension");
            printAsJson($reason);
            setStatusBadRequest();  

            return;
        }

        $contentSize = (int) $_SERVER['CONTENT_LENGTH'];

        $tmpBackupPath = "/tmp/backups";
        if (!is_dir($tmpBackupPath))
            exec("mkdir $tmpBackupPath");

        if ($contentSize < $maxBackupFileSize)
        {
            $backupFile = fopen("$tmpBackupPath/$fileName", "w");
            fwrite($backupFile, $requestBody);
            fclose($backupFile);

            $command = "cd $tmpBackupPath; 7zr x $tmpBackupPath/\"$fileName\" info";
            exec($command, $output, $error);

            if ($error != 0)
            {
                $command = "rm -rf $tmpBackupPath/\"$fileName\" $tmpBackupPath/info";
                exec($command); 

                $reason = array('reason' => "Invalid Archive File");
                printAsJson($reason);

                setStatusBadRequest();
            }
            else
            {
                $output = '';
                exec("/opt/fibaro/scripts/utils/tryDecryptBackup.sh $tmpBackupPath/\"$fileName\"",$output,$error);
                if($error != 0)
                {
                    exec("rm -rf $tmpBackupPath/\"$fileName\" $tmpBackupPath/info");

                    $reason = array('reason' => "Cannot Decrypt Backup");
                    printAsJson($reason);

                    setStatusBadRequest();
                    return;
                }

                $filesystemPath = "/mnt/download/backups";
                if (!is_dir($filesystemPath))
                    exec("mkdir $filesystemPath");
    
                $command = "rm -rf $filesystemPath/*";
                exec($command);

                $infoFile  = file_get_contents("$tmpBackupPath/info");
                $hcSerial  = searchBetween("hcSerial="  , "\n", $infoFile);
                $year      = searchBetween("year="      , "\n", $infoFile);
                $month     = searchBetween("month="     , "\n", $infoFile);
                $day       = searchBetween("day="       , "\n", $infoFile);
                $hour      = searchBetween("hour="      , "\n", $infoFile);
                $minute    = searchBetween("minute="    , "\n", $infoFile);
                $second    = searchBetween("second="    , "\n", $infoFile);
                $newFileName  = "backup_"."$hcSerial"."_"."$year"."_"."$month"."_"."$day"."-"."$hour"."_"."$minute"."_"."$second".".fbi";

                if ($fileName != $newFileName)
                {
                    $command = "mv -f $tmpBackupPath/\"$fileName\" $tmpBackupPath/\"$newFileName\"";
                    exec($command); 
                    $fileName = $newFileName;
                }

                $command = "mv $tmpBackupPath/\"$fileName\" $tmpBackupPath/info $filesystemPath";
                exec($command);

                $command = "sync";
                exec($command); 

                setStatusOK();
            }
        }
        else
        {
            $reason = array('reason' => "Insufficient Storage");
            printAsJson($reason);
            setStatusInsufficientStorage();
        }    
    }
    else
    {
        $reason = array('reason' => "Invalid Content-Type");
        printAsJson($reason);
        setStatusBadRequest();
    }
}

function handleDELETE($type, $id) 
{
    authorize();
    removeBackup($type, $id);
}

// Main function

$requestBody = file_get_contents('php://input');
$requestMethod = $_SERVER['REQUEST_METHOD'];

if ($requestMethod == "GET") 
{
    handleGET();
}
elseif ($requestMethod == "POST") 
{
    handlePOST($requestBody);
}
elseif ($requestMethod == "DELETE") 
{
    parse_str($_SERVER['QUERY_STRING']);
    handleDELETE($type, $id);
}
else 
{
    //TODO check return code in HCServer
    $reason = array('reason' => "Method Not Allowed");
    printAsJson($reason);
    setStatusMethodNotAllowed();
}

?>
