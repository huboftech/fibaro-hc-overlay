<?php
  require_once("/var/www/servicesStatus.php");
  
  header("Content-Type: application/json");
  $response = array();

  $response['HCServer'] = HCServerStatus();
  $response['Zwave'] = ZwaveStatus();
  $response['FibaroServices'] = FibaroServicesStatus();
  $response['RemoteAccess'] = RAStatus();

  echo(json_encode($response));
?>