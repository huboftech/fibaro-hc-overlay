<?php

require("httpStatuses.php");
require("../../authorize.php");

if (!isAuthorized())
{
    sendUnauthorized();
    exit(0);
}

$file = '/tmp/LogsDump';

exec("/usr/bin/createLogsDump.sh -e");

if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="LogsDump.tar.gz"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);

    exec("/usr/bin/createLogsDump.sh --clear");

    exit;
}