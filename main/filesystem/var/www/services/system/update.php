<?php
require_once("../../authorize.php");
require_once("../../instances.php");
require("httpStatuses.php");

function authorize()
{
   if (!isAuthorized() && !isAuthorizedFibaroAuth(array(role::USER, role::INSTALLER))) {
      sendUnauthorized();
      exit(0);
   }
}

function printAsJson($text)
{
    echo(json_encode($text));
}

function sendResponse($error, $result)
{
    header('Content-Type: application/json');
    $ret = array();
    $ret["error"] = $error;
    $ret["result"] = $result;
    printAsJson($ret);
}

function sendError($text)
{
    sendResponse($text, 'error');
}

function sendOk()
{
    sendResponse(NULL, 'ok');
}

function installUpdate($params)
{
    $withoutBackup = '';
    if($params->withoutBackup === 1)  $withoutBackup = '--without-backup';
    exec('/opt/fibaro/scripts/update.sh '.$withoutBackup.' --install &>/dev/null &');
    sendOk();
}

function downloadUpdate($params)
{
    $types = array('stable', 'alpha', 'beta');
    $type = $params->type;

    if (!in_array($type, $types)) {
        http_response_code(400);
    }
    elseif (isset($params->version) and !is_null($params->version)) {
        exec("/opt/fibaro/scripts/update.sh --download $type &>/dev/null &");
        sendOk();
    }
    else {
        if (preg_match("/^(\d\.\d{3})$/", "$params->version")) {
            $version = $params->version;
            exec("/opt/fibaro/scripts/update.sh --download $version &>/dev/null &");
            sendOk();
        }
        else {
            http_response_code(400);
        }
    }
}

function downloadAndInstallUpdate($params)
{
    $types = array('stable', 'alpha', 'beta');
    $type = $params->type;

    $withoutBackup = '';
    if($params->withoutBackup === 1)  $withoutBackup = '--without-backup';
    $installCmd = '/opt/fibaro/scripts/update.sh '.$withoutBackup.' --install &>/dev/null';


    if (!in_array($type, $types)) {
        http_response_code(400);
    }
    elseif (isset($params->version) and !is_null($params->version)) {
        $downloadCmd = "/opt/fibaro/scripts/update.sh --download $type >/dev/null";
        exec("( $downloadCmd  &&  $installCmd ) >/dev/null 2>&1 &");
        sendOk();
    }
    else {
        if (preg_match("/^(\d\.\d{3})$/", "$params->version")) {
            $version = $params->version;
            $downloadCmd = "/opt/fibaro/scripts/update.sh --download $version >/dev/null";
            exec("( $downloadCmd  &&  $installCmd ) >/dev/null 2>&1 &");
            sendOk();
        }
        else {
            http_response_code(400);
        }
    }
}

function cancelDownload() {
    exec('/opt/fibaro/scripts/update.sh --cancel-download &>/dev/null &');
}

function isUpdateRunning() {
	return getNumberOfInstances('update.sh') > 0;
}

function handlePOST($data) {
    authorize();

    $action = $data->action;
    $params = $data->params;

	if($action != "cancelDownload" && isUpdateRunning()) {
		setStatusTooManyRequests();
        return;
	} 
    
    if ($action == "download") {
        downloadUpdate($params);
    }
    elseif ($action == "install") {
        installUpdate($params);
    }
    elseif ($action == "auto") {
       downloadAndInstallUpdate($params);
    }
    elseif ($action == "cancelDownload") {
        cancelDownload();
    }
    else {
        http_response_code(400);
    }
}


// Main function
$requestBody = file_get_contents('php://input');
$requestMethod = $_SERVER['REQUEST_METHOD'];
$data = json_decode($requestBody);

if ($requestMethod == "POST") {
    handlePOST($data);
}
else {
    http_response_code(400);
}
?>
