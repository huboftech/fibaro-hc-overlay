<?php
  require_once("../../utils.php");
  require_once("../../authorize.php");
  
  function handleGet()
  {
    $result = array();
  
    $remoteAccess = getInstallerRemoteAccess();
  
    if ($remoteAccess > time())
    {
        $result['access'] = true;
        $result['remainingTime'] = $remoteAccess - time();
    }
    else
    {
        $result['access'] = false;
    }
    print(json_encode($result));
  }
  
  function handlePut()
  {
    if (!isAuthorizedFibaroAuth(array(role::USER)))
    { // Allow only requests from RA with USER role (owner of HC)
      sendUnauthorized();
      exit(0);
    }
    $requestBody = file_get_contents('php://input');
    $requestJson = json_decode($requestBody);
    
    $installer = getInstaller();

    if (isset($requestJson->installerEmail) && 
        isset($requestJson->installerFirstName)&&
        isset($requestJson->installerLastName))
    {
      $installer['email'] = $requestJson->installerEmail;
      $installer['firstName'] = $requestJson->installerFirstName;
      $installer['lastName'] = $requestJson->installerLastName;
    }
    
    if (isset($requestJson->enableRemoteAccess))
    {
      if ($requestJson->enableRemoteAccess === true)
        $installer['remoteAccess'] = time() + 86400;
      else
        $installer['remoteAccess'] = 0;
    }

    updateInstaller($installer);
  }
  
  $requestMethod = $_SERVER['REQUEST_METHOD'];

  if($requestMethod == "GET")
  {
    handleGET();
  }
  elseif ($requestMethod == "PUT") 
  {
    handlePut();
  }

?>