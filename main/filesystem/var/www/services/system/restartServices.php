<?php

require("../../authorize.php");

function sendAccepted()
{
    exec("/etc/init.d/S90fibaro-start restart");
    header("Location: index.html");
    header('HTTP/1.1 202 Accepted');
    exit;
}

if (isAuthorized() || isAuthorizedFibaroAuth(array(role::USER, role::INSTALLER)))
{
    sendAccepted();
}
else
{
    sendUnauthorized();
}

?>
