<?php
  require("../../localization.php");
  require("httpStatuses.php");
  require("../../authorize.php");

  //error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

  function authorize()
  {
    if (!isAuthorized())
    {
      sendUnauthorized();
      exit(0);
    }
  }

  function readFileAndTrim($filePath)
  {
    $fileHandle = fopen($filePath, "r") or die("");
    $fileContent = fread($fileHandle, filesize($filePath));
    $fileContent = trim($fileContent);

    fclose($fileHandle);
    return $fileContent;
  }

  function parseStatusFile($lang)
  {
    $SYSTEM_STATUS_FILE_PATH="/opt/fibaro/system/status";
    $ret = json_decode(readFileAndTrim($SYSTEM_STATUS_FILE_PATH));

    $ret->{'description'} = getMsg($lang, $ret->{'description'});
    $ret->{'title'} = getMsg($lang, $ret->{'title'});
    if ($ret->{'stage'} !== NULL) {
    	$ret->stage->description = getMsg($lang, $ret->stage->description);
    }
    return $ret;
  }

  function printAsJson($text)
  {
    echo(json_encode($text));
  }

  function handleGET()
  {
    $lang = $_GET["lang"];

    header('Content-Type: application/json');
    $response = parseStatusFile($lang);
    printAsJson($response);
    //setStatusOk();
  }

  function servicesAreRunning()
  {
    $returnStatus = 0;
    exec("/etc/init.d/S90fibaro-start status" , $output, $returnStatus);
    return $returnStatus == 0;
  }

  function handlePOST()
  {
    authorize();
    $action = $_POST["action"];

    if ($action != "clearError")
    {
      setStatusBadRequest();
      return;
    }
    
    if(servicesAreRunning())
      exec(". /opt/fibaro/system/status-utils && set_status_idle");
    else
      exec("systemReboot");
    setStatusOk();
  }

  $requestMethod = $_SERVER['REQUEST_METHOD'];

  if ($requestMethod == "GET")
  {
    handleGET();
  }
  elseif ($requestMethod == "POST")
  {
    handlePOST();
  }
  else
  {
    setStatusMethodNotAllowed();
  }
?>
