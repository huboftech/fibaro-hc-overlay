<?php

require("authorize.php");

function sendAccepted()
{
    exec("/etc/init.d/S90fibaro-start restart");
    header("Location: index.html");
    exit;
}

if (isAuthorized())
{
    sendAccepted();
}
else
{
    sendUnauthorized();
}

?>
