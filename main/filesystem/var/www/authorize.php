<?php

require_once("utils.php");
require_once("JWTParser.php");

class role
{
  const USER = 'USER';
  const INSTALLER = 'INSTALLER';
}

function isLocalRequest()
{
    $ipAddress = "";
    if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
        $ipAddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else
        $ipAddress = $_SERVER['REMOTE_ADDR'];

    $whitelist = array( '127.0.0.1', '::1' );
    if(in_array($ipAddress, $whitelist))
        return true;

    return false;
}

function isAuthorized()
{
    $hash = getProperty(2, 'Pwd');

    if (!isset($_SERVER['PHP_AUTH_USER']))
    {
        return false;
    }
    else
    {
        $username = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];

        $calculatedHash = sha1("Ogp4lmKhH6" . $password);

        if ($hash != $calculatedHash)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

function getRaPublicKey()
{
  if (file_exists("/tmp/ra-key.pub")) // cache RA public key to reduce RA requests
    return file_get_contents("/tmp/ra-key.pub");

  $options = array(
    CURLOPT_RETURNTRANSFER => true,     // return web page
    CURLOPT_HEADER         => false,    // don't return headers
    CURLOPT_ENCODING       => "",       // handle all encodings
    CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
    CURLOPT_TIMEOUT        => 120,      // timeout on response
  );

  $ch      = curl_init( "https://ra-api.fibaro.com/api/1.0/auth/tokenKey" );
  curl_setopt_array( $ch, $options );
  $content = curl_exec( $ch );
  $err     = curl_errno( $ch );
  $errmsg  = curl_error( $ch );
  $header  = curl_getinfo( $ch );
  curl_close( $ch );
    
  if ($err)
  {
    file_put_contents("/tmp/authorize.log", date("Y-m-d H:i:s") . "  " . $errmsg, FILE_APPEND);
    return false;
  }

  $key = false;
  if (isset($header['http_code']) && $header['http_code'] == 200)
  {
    $key = json_decode($content);
    if ($key && isset($key->tokenKey))
    {
      $key = $key->tokenKey;
      file_put_contents("/tmp/ra-key.pub", $key);
    }
  }
  return $key;
}

function isAuthorizedFibaroAuth($allowedRoles = array("USER"))
{
  $tokenHeader = $_SERVER['HTTP_X_FIBARO_AUTH'];
  $token = explode(" ", $tokenHeader);

  if(count($token) != 2)
    return false;
  if ($token[0] != "RA-Access-Token")
    return false;
    
  $key = getRaPublicKey();

  $jwtParser = new JWTParser;
  $parserResult = $jwtParser->parseJWT($token[1], $key);
  
  if ($parserResult)
    $parserResult = $jwtParser->verifyTokenInRa(); // TODO: maybe cache token to reduce requests to RA??

  if($parserResult == true)
  {
    $tmp = split("\.", $token[1]);
    $jsonToken = json_decode(base64_decode($tmp[1]));
    $role = $jsonToken->user_role;
    return authorizeRole($role, $allowedRoles);
  }

  return false;
}

function authorizeRole($role, $allowedRoles)
{
  if (!in_array($role, $allowedRoles))
    return false;

  return true;
}

function sendUnauthorized()
{
    header('WWW-Authenticate: Basic realm="fibaro"');
    header('HTTP/1.1 401 Unauthorized');
    exit;
}

?>
