<?php

function createSession($dbPath = '/mnt/user_data/db')
{
	$dbh = new SQLite3($dbPath);
	$dbh->busyTimeout(5 * 1000); // ms

	return $dbh;
}

?>
