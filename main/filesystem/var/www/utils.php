<?php
require_once("/var/www/dbSession.php");

function getSerial()
{
  return exec(". /opt/fibaro/scripts/config ; getSerial");
}

function getHwKey()
{
  return exec(". /opt/fibaro/scripts/config ; getHWKey");
}

function getInstallerRemoteAccess()
{
  $db = createSession();
  $result = $db->querySingle("SELECT remoteAccess FROM installer", true);
  return $result['remoteAccess'];
}

function getProperty($deviceId, $propertyName)
{
  $db = createSession();
  $statement = $db->prepare("SELECT Value FROM NEW_Property WHERE Name=:name AND Device_Id = :id");
  $statement->bindValue(':id', $deviceId);
  $statement->bindValue(':name', $propertyName);
  
  $result = $statement->execute();
  $result = $result->fetchArray();
  return json_decode($result['Value']);
}

function getInstaller()
{
  $db = createSession();
  $result = $db->querySingle("SELECT * FROM installer", true);
  return $result;
}

function updateInstaller($installer)
{
  $db = createSession();
  $statement = $db->prepare("update installer set email=:email, firstName=:firstName, lastName=:lastName, remoteAccess=:remoteAccess");
  $statement->bindValue(':email', $installer['email']);
  $statement->bindValue(':firstName', $installer['firstName']);
  $statement->bindValue(':lastName', $installer['lastName']);
  $statement->bindValue(':remoteAccess', $installer['remoteAccess']);
  
  $statement->execute();
}

function getLowBatteryDevices()
{
  $db = createSession();

  $lowBetteryLevelZwaveDevices = "SELECT D.Id, D.Device_type, D3.Name, R.Name as Room_name, CAST(P.Value AS INT) as Battery_level, CAST(P3.value AS INT) as Parameters_template" .
    " FROM new_device D" .
    " JOIN (SELECT parent_id, min(id) as id FROM new_device WHERE visible=1 GROUP BY parent_id) as D2 on D2.parent_id=D.id" .
    " JOIN new_device D3 on D2.id=D3.id" .
    " JOIN new_property P on P.device_id=D.id AND P.name='batteryLevel'" .
    " JOIN new_property P2 on P2.device_id=D.id AND P2.name='dead' AND P2.value='false'" .
    " JOIN new_property P3 on P3.device_id=D.id AND P3.name='parametersTemplate'" .
    " LEFT JOIN room R on D3.room_id = R.pk_room" .
    " WHERE P.value in(0,255) AND D.device_type='com.fibaro.zwaveDevice'";

  $dbResult = $db->query($lowBetteryLevelZwaveDevices);

  $result = array();
  
  $row = array();
  while ($row = $dbResult->fetchArray(SQLITE3_ASSOC)) 
    $result[] = $row;

  return $result;
}

?>