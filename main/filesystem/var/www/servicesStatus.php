<?php
  function isProcessRunning($processName)
  {
     exec("ps -Af | grep $processName | grep -v grep", $pids);
     return count($pids) != 0;
  }

  function statusBasedOnProcessRunning($processName)
  {
    if (!isProcessRunning($processName))
      return array("running"=>false, "status"=>"Process not running");

    return array("running"=>true, "status"=>"Ok");
  }

  function HCServerStatus()
  {
    $result = statusBasedOnProcessRunning("HCServer");

    if ($result["running"])
    {
      $settingsInfo = file_get_contents("http://localhost/api/settings/info");
      if (!$settingsInfo)
        $result = array("running"=>false, "status"=>"HTTP api not responding");
    }

    return $result;
  }

  function ZwaveStatus()
  {
    return statusBasedOnProcessRunning("Zwave");
  }

  function FibaroServicesStatus()
  {
    return statusBasedOnProcessRunning("ActionHandler.lua");
  }

  function RAStatus()
  {
    return statusBasedOnProcessRunning("RemoteAccess");
  }
