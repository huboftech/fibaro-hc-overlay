<?php

require_once("/var/www/utils.php");

class JWTParser
{
  private $jwtToken = '';
  private $token;
  private $tokenParts = '';
  private $hash = '';

  private $key;
  private $algorithm = '';
  private $exp = 0;
  private $userRole = '';
  private $clientId = '';
      
  function parseJWT($jwt, $key)
  {
    $this->key = $key;
    if (empty($jwt) || empty($key))
      return false;
      
    $this->jwtToken = $jwt;
    $this->tokenParts = split('\.', $jwt);

    if (count($this->tokenParts) != 3)
      return false;
      
    $token = $this->tokenParts[0] . "." . $this->tokenParts[1];
    
    $this->hash = base64_decode(strtr($this->tokenParts[2], '-_', '+/'));
    return $this->parseHead(base64_decode($this->tokenParts[0])) &&
           $this->parsePayload(base64_decode($this->tokenParts[1])) &&
           $this->verifyTimeout() &&
           $this->verifySignature();

    return false;
  }

  function parseHead($head)
  {
    $head = json_decode($head);

    if (is_null($head))
      return false;

    if (isset($head->alg))
      $this->algorithm = $head->alg;
    else
      return false;

    return true;
  }
  
  function parsePayload($payload)
  {
    $payload = json_decode($payload);

    if (is_null($payload))
        return false;

    if (isset($payload->exp))
        $this->exp = $payload->exp;

    $this->userRole = $payload->user_role;

    $this->clientId = $payload->client_id;

    return true;
  }
  
  function verifySignature()
  {
    if ($this->algorithm == "RS256")
      return $this->verifyRS256();

    return false;

  }
  
  function verifyRS256()
  {
    $data = $this->tokenParts[0] . '.' .  $this->tokenParts[1];
    $key = $this->key;

    $result = openssl_verify($data,
                             $this->hash,
                             $key,
                             "sha256");

    return $result === 1;
  }
  
  function verifyTimeout()
  {
    return time() < $this->exp;
  }
  
  function verifyTokenInRa()
  {
    $uri = "https://ra-api.fibaro.com/api/1.0/device/access";

    $serial = getSerial();
    $hwKey = getHwKey();
    $auth = $serial . ':' . $hwKey;
    $tokenHeader = 'X-Fibaro-Auth: RA-Access-Token ' . $this->jwtToken;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $uri);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array($tokenHeader));
    curl_setopt($ch, CURLOPT_USERPWD, $auth);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = trim(curl_exec($ch));
    return $result === "true";
  }
}
?>