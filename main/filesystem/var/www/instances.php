<?php

function getNumberOfInstances($filename) {
    exec('ps -A | grep ' . escapeshellarg($filename) , $results);
    return count($results) - 2;  // Including "ps" and "grep" calls from above
}

?>
