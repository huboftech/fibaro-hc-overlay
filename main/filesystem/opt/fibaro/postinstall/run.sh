#!/bin/sh

. /opt/fibaro/scripts/config

sqlite3 "${configDbPath}" "UPDATE HC_Data SET firstRun=1"
sqlite3 "${configDbPath}" "UPDATE HC_Data SET firstRunAfterUpdate=1"

