#!/bin/sh

#
# Created by a.przybecki@fibargroup.com on 23.09.16.
#
. /opt/fibaro/scripts/config

hcSerialNumber=$(getSerial)
hcVersion=$(getVersion)
hcVersionType=$(getVersionType)
hcDefaultLanguage=$(getDefaultLanguage)
updateURL="${urlSoft}"

echo "Serial: $hcSerialNumber"
echo "Version: $hcVersion"
echo "Uri: $updateURL"
echo "DefaultLanguage: $hcDefaultLanguage"

cmd="${1//-}"
#Random wait for check update
if [ "$cmd" = "random" ]
then
    RAN=$(shuf -i 0-100 -n 1)
    echo "waiting ${RAN} s"
    sleep "$RAN"
fi

version=$(/opt/fibaro/scripts/utils/updateAvailableVersion.sh "${hcSerialNumber}" "${hcVersion}" "${updateURL}" "${hcDefaultLanguage}")
echo "${version}"

stable=$(echo "${version}" | jq ".stable" | tr -d \".)
beta=$(echo "${version}" | jq ".beta" | tr -d \".)
alpha=$(echo "${version}" | jq ".alpha" | tr -d \".)
dotlessVersion=$(echo "${hcVersion}" | tr -d .)

#shellcheck disable=SC2086
if [ ${stable} -gt ${dotlessVersion} ]
then
    echo "GA update available"
    led_update blink_slow
elif [ ${hcVersionType} != "stable" ] && [ ${beta} -gt ${dotlessVersion} ]
then
    echo "Beta update available"
    led_update blink_slow
elif [ ${hcVersionType} == "alpha" ] && [ ${alpha} -gt ${dotlessVersion} ]
then
    echo "Alpha update available"
    led_update blink_slow
else
    echo "No update available"
    led_update off
    exit $status
fi
