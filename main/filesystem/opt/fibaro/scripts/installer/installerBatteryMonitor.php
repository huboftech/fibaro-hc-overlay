<?php
require_once("sendMessageToInstaller.php");
require_once("/var/www/utils.php");

$installer = getInstaller();

if (empty($installer['email']))
{
  exit;
}

echo "\n\n" . date("Y-m-d H:i:s") . " : Starting script\n";

$devices = getLowBatteryDevices();
if (count($devices))
{
  $context = array("lowBatteryDevices"=> $devices);

  sendMessageToInstaller($installer, "LOW_BATTERY", $context);
}



