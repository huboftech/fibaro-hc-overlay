<?php
require_once("/var/www/servicesStatus.php");
require_once("/var/www/systemStatus.php");
require_once("/var/www/utils.php");
require_once("sendMessageToInstaller.php");

$crashStatusFileName = "/tmp/.servicesStatus";
$installer = getInstaller();

function setServicesStatus($status)
{
  global $crashStatusFileName;
  file_put_contents($crashStatusFileName, $status);
}

function getServicesStatus()
{
  global $crashStatusFileName;
  if (!file_exists($crashStatusFileName))
  {// file doesn't exists only on system startup - assume OK
    file_put_contents($crashStatusFileName, "OK");
  }
  
  return file_get_contents($crashStatusFileName);
}

if (empty($installer['email']))
{
  exit;
}

echo "\n\n" . date("Y-m-d H:i:s") . " : Starting script\n";

$crashedServices = array();

$status = HCServerStatus();
if ($status['status'] != 'Ok')
  $crashedServices[] = array('service'=>'HCServer');

$status = ZwaveStatus();
if ($status['status'] != 'Ok')
  $crashedServices[] = array('service'=>'Zwave');

$status = FibaroServicesStatus();
if ($status['status'] != 'Ok')
  $crashedServices[] = array('service'=>'FibaroServices');

$servicesStatus = getServicesStatus();
$systemStatus = parseStatusFile();

// If system is updating/backing up/restoring services are down, so do not perform check
$excludedStatuses = array('STATUS_BACKUP_CREATE', 'STATUS_BACKUP_RESTORE', 'STATUS_SYSTEM_UPDATING');
if (in_array($systemStatus['status'], $excludedStatuses))
  return;

echo "Last services status: " . $servicesStatus;
echo "\nCurrent system status:  " . $systemStatus['status'];
echo "\nCrashed services: " . print_r($crashedServices, true) . "\n";


// SYSTEM RESTORED AFTER PREVIOUS CRASH
if (!$crashedServices && $servicesStatus!="OK")
{
  setServicesStatus("OK");
  echo "System restored\n";
  sendMessageToInstaller($installer, "SERVICE_RESTORED", new ArrayObject());
  exit;
}

// SOME SERVICES ARE DOWN - MARK FOR CHECK IN NEXT SCRIPT RUN
if ($crashedServices && $servicesStatus=="OK")
{ // some service crash detected
  setServicesStatus("CHECK");
  echo "System potentialy crashed\n";
  exit;
}

// SYSTEM WAS MARKED FOR CHECK AND CRASH CONDITION IS STILL VALID
if ($crashedServices && $servicesStatus=="CHECK")
{ // some service crash detected
  setServicesStatus("CRASH");
  echo "System crashed\n";
  sendMessageToInstaller($installer, "SERVICE_FAULT", array("crashedServices"=>$crashedServices));
  exit;
}
