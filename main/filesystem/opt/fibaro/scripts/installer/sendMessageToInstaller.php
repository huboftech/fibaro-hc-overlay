<?php
function sendMessageToInstaller($installer, $messageType, $data)
{
  $message = array();
  $message['types'] = array("email", "push");
  $message['topic'] = $messageType;
  $message['recipients'] = array(array("email"=>$installer["email"], "role"=>"INSTALLER"));
  $message['context'] = $data;
  
  $message = json_encode($message);
  echo "Sending email: " .  $message . "\n";

  $serial = getSerial();
  $hwKey = getHwKey();
  $auth = $serial . ':' . $hwKey;

  $options = array(
    CURLOPT_CUSTOMREQUEST  => "POST",
    CURLOPT_POSTFIELDS     => $message,
    CURLOPT_RETURNTRANSFER => true,     // return web page
    CURLOPT_HEADER         => false,    // don't return headers
    CURLOPT_ENCODING       => "",       // handle all encodings
    CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
    CURLOPT_TIMEOUT        => 120,      // timeout on response
    CURLOPT_HTTPHEADER     => array('Content-Type:application/json', 'Content-Length: ' . strlen($message)),
    CURLOPT_USERPWD        => $auth,
  );
  
  $ch      = curl_init( "https://ra-api.fibaro.com/api/1.0/notifications" );
  curl_setopt_array( $ch, $options );
  $content = curl_exec( $ch );
  $err     = curl_errno( $ch );
  $errmsg  = curl_error( $ch );
  $header  = curl_getinfo( $ch );
  curl_close( $ch );

  echo "Received status: " .  $header["http_code"] . "\n";
  echo "Received content: " .  $content . "\n";
}
