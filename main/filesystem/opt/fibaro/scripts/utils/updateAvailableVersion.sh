#!/bin/sh
. /opt/fibaro/system/status-utils

[ $# -ne 4 ] && { echo "updateAvailableVersion.sh invalid number of params:$#"; exit 1; }

serialNumber="${1}"
softVersion="${2}"
uriUpdateServer="${3}/info?output=json"
defaultLanguage="${4}"

version="$(curl -s -H "Content-Type:application-json" \
                   -H "X-Fibaro-Soft-Version:${softVersion}" \
                   -H "X-Fibaro-Serial-Number:${serialNumber}" \
                   -H "X-Fibaro-Default-Language:${defaultLanguage}" "${uriUpdateServer}" | \
           jq ". | {stable: .stable.version, beta: .beta.version, alpha: .alpha.version}")"

echo "${version}"
set_api_update_endpoint "${version}"
