#!/bin/sh

. /opt/fibaro/scripts/config

fileName=$1

errorCode=$(7zr -so x "${fileName}" backup.7z | backupTool decrypt_backup -h "$(getHWKey)" -s "$(getHDDserial)" -k "$(cat "${fileBTKey}")" -o /dev/null)

if ! ${errorCode}; then
  echo "Decryption error: ${errorCode}"
  exit 1
fi
