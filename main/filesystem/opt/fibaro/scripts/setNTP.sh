#!/bin/sh
. /opt/fibaro/scripts/config

NTP_START_SCRIPT="/etc/init.d/S51ntp"

setNtpServer()
{
	ntpServer=$1
	$NTP_START_SCRIPT stop
	sleep 1
	#replace ntp server address in configuration file
	echo "server $ntpServer iburst" > "${ntpConfPath}"
	sync
	$NTP_START_SCRIPT start
}

usage()
{
echo "USAGE:"
echo "   setNTP.sh [--ntp <NTP SERVER ADDRESS> | --manual <YYYY-MM-DD> <hh:mm>]"
echo ""
echo "DESCRIPTION:"
echo "   Sets system date and time according to either a given ntp server (using ntpd), or a given date."
echo ""
echo "EXAMPLES:"
echo "   setNTP.sh --manual \"2011-01-31 12:24\""
echo "   setNTP.sh --ntp \"pool.ntp.org\""
echo ""
echo "EXIT STATUS:"
echo "   0 - SUCCESSFUL"
echo "   1 - help message (you're reading it at the moment)"
echo "   2 - script must be run as root"
exit 1
}

if [ "$#" -ne 2 ]; then
   echo "Illegal number of parameters."
   usage
fi
if [ "$(id -u)" -ne "0" ] ; then
   echo "Please run this script as root."
   exit 2
fi
if [ "$1" = "--ntp" ]
then
   echo "Setting date from NTP server: $2..."
   setNtpServer "$2"
elif [ "$1" = "--manual" ]
then
   echo "Setting manual date $2..."
   $NTP_START_SCRIPT stop
   date "+%d-%m-%Y %H:%M" -s "$2"
else
   echo "Bad parameters."
   usage
fi

hwclock -w -u

exit 0
