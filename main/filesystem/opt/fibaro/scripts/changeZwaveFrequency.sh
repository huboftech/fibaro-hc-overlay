#!/bin/sh

. /opt/fibaro/scripts/config

set -e

if [ $# -ne 1 ] ; then
  echo "usage: $0 <path-to-zwave-flash-file>"
  exit 1
fi

filePath=$(readlink -f "${1}")
filename=$(basename "${filePath}")
frequency="${filename%.*}"

if [ -f "${filePath}" ] ; then
	/opt/fibaro/scripts/setZwaveFrequency.sh "${frequency}" || { echo "Error: invalid frequency"; exit 3; }

	#stop Zwave process so that it won't interfere with ZwSerial
	zwaveKilled=1
	if isProcessRunning Zwave ; then
		${fibaroStart} stop Zwave
	else 
		zwaveKilled=0
	fi

	echo "Writing Z-Wave flash memory..."
	writeZwaveFlash "${filePath}" || echo "Error: Could not write into Z-Wave flash memory."

	#restart Zwave if it was killed
	if [ ${zwaveKilled} -eq 1 ]; then
		${fibaroStart} start Zwave
	fi
else
  echo "Error: file doesn't exist"
  exit 2
fi
