#!/bin/sh

if ! kill -0 "$(cat /var/run/lighttpd.pid)" 2>/dev/null; then
  echo "Restaring lighttpd..."
  /etc/init.d/S50lighttpd restart
fi
