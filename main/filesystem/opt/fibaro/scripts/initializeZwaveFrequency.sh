#!/bin/sh
. /opt/fibaro/scripts/config

frequency=$(getZwaveFrequency)
echo "Frequency: ${frequency}"

sqlExec "INSERT OR REPLACE INTO NEW_Property(Device_Id, Name, Value) VALUES(1, 'zwaveRegion', '$frequency')"
