#!/bin/sh

#include
. /opt/fibaro/scripts/config
. /opt/fibaro/scripts/libs/history_limits.sh

dbPath="${configDbPath}"

if [ ! -z "$1" ]
then
        dbPath="$1"
fi

shrinkTable()
{
        # $1 - primary key
        # $2 - table
        # $3 - limit

        queryResult=$(sqlite3 "${dbPath}" "SELECT MIN($1) as minVal, MAX($1) as maxVal from $2")
        minVal=$(echo "${queryResult}" | cut -d '|' -f 1)
        maxVal=$(echo "${queryResult}" | cut -d '|' -f 2)

        aboveLimit=$((maxVal - minVal - $3))

        if [ ${aboveLimit} -gt 0 ]
        then
                lastId=$((minVal + aboveLimit))
                sqlite3 "${dbPath}" "DELETE FROM ${2} WHERE ${1} <= $lastId"
                
                echo "Table '$2' was shrinked to $3 rows ($aboveLimit deleted)"
        fi
}

echo "Performing database cleaning (path=$dbPath) ..."

shrinkTable "id" "Mobile_Location" "${Mobile_Location_limit}"
shrinkTable "id" "NEW_Device_History" "${NEW_Device_History_limit}"
shrinkTable "id" "NEW_Power_History" "${NEW_Power_History_limit}"
shrinkTable "id" "NEW_Temperature_History" "${NEW_Temperature_History_limit}"
shrinkTable "id" "NEW_Smoke_History" "${NEW_Smoke_History_limit}"
shrinkTable "id" "Audit_Logs" "${Audit_Logs_limit}"

sqlite3 "${dbPath}" "vacuum"
