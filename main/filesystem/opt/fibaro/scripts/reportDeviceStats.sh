#!/bin/sh

. /opt/fibaro/scripts/config

serial=$(getSerial)
hwKey=$(getHWKey)
version=$(cat "${fileVersion}")
headers=""

stats=$(curl http://127.0.0.1:11111/api/devices/analytics 2>/dev/null)

curl -X POST \
    --header X-Fibaro-Serial-Number:"$serial" \
    --header X-Fibaro-Soft-Version:"$version" \
    --header X-Fibaro-HWKey:"$hwKey" \
    -d "'${stats}'" \
    "${urlSoft}/${infoUrl}"