insert or replace into LogTranslation values ("pl", "INSTALLER_MONITORING_REQUEST_TITLE", "Prośba od Instalatora");
insert or replace into LogTranslation values ("en", "INSTALLER_MONITORING_REQUEST_TITLE", "Installer Request");
insert or replace into LogTranslation values ("de", "INSTALLER_MONITORING_REQUEST_TITLE", "Anfrage des Installateur");

insert or replace into LogTranslation values ("pl", "INSTALLER_MONITORING_REQUEST_BODY", "prosi o zgodę na monitorowanie statusu Home Center o numerze seryjnym");
insert or replace into LogTranslation values ("en", "INSTALLER_MONITORING_REQUEST_BODY", "asks for permission to monitor status of the Home Center with the serial number");
insert or replace into LogTranslation values ("de", "INSTALLER_MONITORING_REQUEST_BODY", "bittet um Erlaubnis, den Status des Home Centers mit der Seriennummer zu überwachen");

insert or replace into LogTranslation values ("pl", "INSTALLER_REMOTE_ACCESS_REQUEST_TITLE", "Prośba od Instalatora");
insert or replace into LogTranslation values ("en", "INSTALLER_REMOTE_ACCESS_REQUEST_TITLE", "Installer Request");
insert or replace into LogTranslation values ("de", "INSTALLER_REMOTE_ACCESS_REQUEST_TITLE", "Anfrage des Installateur");

insert or replace into LogTranslation values ("pl", "INSTALLER_REMOTE_ACCESS_REQUEST_BODY", "prosi o zgodę na dostęp zdalny do Home Center o numerze seryjnym");
insert or replace into LogTranslation values ("en", "INSTALLER_REMOTE_ACCESS_REQUEST_BODY", "asks for permission to have a remote access to the Home Center with the serial number");
insert or replace into LogTranslation values ("de", "INSTALLER_REMOTE_ACCESS_REQUEST_BODY", "bittet um Erlaubnis, einen Fernzugriff auf das Home Center mit der Seriennummer zu haben");
