create table if not exists push_messages(
  id integer not null primary key,
  devices text not null,
  sceneId int not null,
  created int not null
);

alter table push_messages add column service text;
alter table push_messages add column method text;
alter table push_messages add column param text;

update push_messages set service='SceneService', method='Run', param='{"sceneId":'||sceneId||'}';

create table push_messages_copy(
  id integer not null primary key,
  devices text not null,
  created int not null,
  service text not null,
  method text not null,
  param text not null
);

insert into push_messages_copy(id, devices, created, service, method, param) select id, devices, created, service, method, param from push_messages;
drop table push_messages;
alter table push_messages_copy rename to push_messages;