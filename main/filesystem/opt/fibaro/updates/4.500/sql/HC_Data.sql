BEGIN TRANSACTION;

ALTER TABLE HC_Data RENAME TO HC_Data_tmp;

CREATE TABLE 'HC_Data' (
   'Serial_Number' TEXT NOT NULL,
   'HW_Key' TEXT NOT NULL,
   'Version' TEXT NOT NULL,
   'ZW_Version_Chip' TEXT NOT NULL,
   'Hotel_Mode' INTEGER NOT NULL DEFAULT '0',
   'Remote_Access' INTEGER NOT NULL DEFAULT '1',
   'Version_Type' TEXT,
   'powerPrice' REAL,
   'powerCurrency' TEXT,
   'batteryLowNotification' INTEGER,
   'defaultTemperatureSensor' INTEGER,
   'defaultHumiditySensor' INTEGER,
   'defaultLightSensor' INTEGER,
   'fireAlarmTemperature' REAL NOT NULL DEFAULT '60',
   'freezeAlarmTemperature' REAL NOT NULL DEFAULT '1',
   'smsManagement' INTEGER NOT NULL DEFAULT '0',
   'frequency' TEXT NOT NULL DEFAULT 'EU',
   'HW_version' TEXT NOT NULL DEFAULT '1.0',
   'firstRun' BOOLEAN NOT NULL DEFAULT '1',
   'alarmEnabled' BOOLEAN NOT NULL DEFAULT '1',
   'weatherProviderId' INTEGER NOT NULL DEFAULT '3',
   'name' TEXT,
   'firstRunAfterUpdate' BOOLEAN NOT NULL DEFAULT 1,
   'Remote_Access_Support' INTEGER DEFAULT 0 NOT NULL);

INSERT INTO HC_Data(Serial_Number, HW_Key, Version, ZW_Version_Chip, Hotel_Mode, Remote_Access, powerPrice,
                    powerCurrency, batteryLowNotification, defaultTemperatureSensor, defaultHumiditySensor, 
                    defaultLightSensor, fireAlarmTemperature, freezeAlarmTemperature, smsManagement,
                    frequency, HW_version, firstRun, alarmEnabled, weatherProviderId,
                    name, firstRunAfterUpdate, Remote_Access_Support)
SELECT Serial_Number, HW_Key, Version, ZW_Version_Chip, Hotel_Mode, Remote_Access, powerPrice,
                    powerCurrency, batteryLowNotification, defaultTemperatureSensor, defaultHumiditySensor, 
                    defaultLightSensor, fireAlarmTemperature, freezeAlarmTemperature, smsManagement,
                    frequency, HW_version, firstRun, alarmEnabled, weatherProviderId,
                    name, firstRunAfterUpdate, Remote_Access_Support
FROM HC_Data_tmp;

DROP TABLE HC_Data_tmp;

COMMIT;
