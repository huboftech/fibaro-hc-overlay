#!/bin/sh

. /opt/fibaro/scripts/config

DIR=$(getFullDirName "${0}")

if [ -d "${DIR}" ]; then
  sqlExecByDir "${DIR}"/sql
fi

rm -f /mnt/user_data/liliDB
