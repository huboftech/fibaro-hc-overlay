CREATE TABLE Audit_Logs (
    id INTEGER NOT NULL PRIMARY KEY,
    host TEXT NOT NULL, 
    userId INTEGER NOT NULL, 
    serviceName TEXT NOT NULL, 
    setting TEXT NOT NULL, 
    oldValue TEXT NOT NULL, 
    newValue TEXT NOT NULL,
    timestamp INTEGER NOT NULL
);