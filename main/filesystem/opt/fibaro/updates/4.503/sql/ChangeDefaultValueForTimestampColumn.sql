BEGIN;

ALTER TABLE [NEW_Property] RENAME TO [NEW_Property_old];

CREATE TABLE [NEW_Property] (
    [Device_Id] INTEGER NOT NULL, 
    [Name] TEXT NOT NULL, 
    [Value] TEXT NOT NULL, 
    [Modification_Time] INTEGER NOT NULL DEFAULT (strftime('%s', 'now')), 
    PRIMARY KEY ([Device_Id], [Name]));

DROP INDEX "NEW_Property_fk_Property_Device";

INSERT INTO [NEW_Property] ([Device_Id], [Name], [Value]) 
    SELECT [Device_Id], [Name], [Value] 
    FROM [NEW_Property_old];

CREATE INDEX "NEW_Property_fk_Property_Device" ON [NEW_Property] ( "Device_Id" );

DROP TABLE [NEW_Property_old];

COMMIT;
