create table IF NOT EXISTS 'SelfCheckResult'(
    'id' integer not null primary key AUTOINCREMENT,
    'startTimestamp' integer not null default '0',
    'finishTimestamp' integer not null default '0'
);

create table IF NOT EXISTS 'StepResult'(
    'id' integer not null primary key AUTOINCREMENT,
    'step' text not null,
    'status' text not null,
    'progress' integer not null,
    'result' text,
    'stepStartTimestamp' integer not null default '0',
    'stepFinishTimestamp' integer not null default '0',
    'finished' integer not null default '0',
    'selfCheckResultId' integer not null,
    foreign key ('selfCheckResultId') references 'SelfCheckResult' ('id') on delete cascade
);