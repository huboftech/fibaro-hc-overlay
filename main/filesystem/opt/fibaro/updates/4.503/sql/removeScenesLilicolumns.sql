BEGIN;

CREATE TABLE "NEW_Scene_Copy" (
  "Id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "Enabled" numeric NOT NULL DEFAULT '1',
  "Autostart" numeric NOT NULL DEFAULT '0',
  "Type" text NOT NULL DEFAULT '0',
  "Name" text NOT NULL,
  "Room_Id" integer NOT NULL DEFAULT '0',
  "Lua" numeric NULL DEFAULT '0',
  "Icon_Id" integer NOT NULL DEFAULT '0',
  "Visible" integer NULL,
  "ReadOnly" numeric NOT NULL DEFAULT '0',
  "Killable" integer NULL DEFAULT '0',
  "protectedByPin" integer NULL DEFAULT '0',
  "properties" numeric NULL DEFAULT '""',
  "max_instances" integer NOT NULL DEFAULT '2',
  "RunConfig" TEXT NULL, 
  "actions" TEXT);

INSERT INTO NEW_Scene_Copy (Id, Enabled, Autostart, Type, Name, Room_Id, Lua, Icon_Id, Visible, ReadOnly, Killable, protectedByPin, properties, max_instances, RunConfig, actions)
SELECT Id, Enabled, Autostart, Type, Name, Room_Id, Lua, Icon_Id, Visible, ReadOnly, Killable, protectedByPin, properties, max_instances, RunConfig, actions FROM NEW_Scene;

DROP TABLE NEW_Scene;
ALTER TABLE NEW_Scene_Copy RENAME TO NEW_Scene;

COMMIT;

