insert or replace into LogTranslation values ("en", "TXT_RECOMMEND_AFTER_MIGRATION_BACKUP_TITLE", "Create a backup");
insert or replace into LogTranslation values ("pl", "TXT_RECOMMEND_AFTER_MIGRATION_BACKUP_TITLE", "Utwórz backup");

insert or replace into LogTranslation values ("en", "TXT_RECOMMEND_AFTER_MIGRATION_BACKUP_TEXT", "All your backups created so far are incompatible. It is recommended to create a new one.");
insert or replace into LogTranslation values ("pl", "TXT_RECOMMEND_AFTER_MIGRATION_BACKUP_TEXT", "Wszystkie utworzone do tej pory backupy są niekompatybilne. Rekomendujemy stworzenie nowego backupu.");