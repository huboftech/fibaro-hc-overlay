#!/bin/sh

if [ $# -ne 1 ] ; then
  echo "usage: $0 <path-to-zwave-flash-file>"
  exit 1
fi

/opt/fibaro/scripts/changeZwaveFrequency.sh "${1}"