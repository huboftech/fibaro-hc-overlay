cmake_minimum_required(VERSION 2.8)

message("Adding main system files")

add_subdirectory(filesystem)
add_subdirectory(src)
if (NOT DEFINED WITHOUT_EXTERNAL_PACKAGES)
  add_subdirectory(external)
endif()
add_subdirectory(scripts)
