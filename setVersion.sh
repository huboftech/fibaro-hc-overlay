#!/bin/sh

ETC_DIR="${1}/etc"
VERSION="${2}"
SYSTEM_TYPE="${3}"

case "${SYSTEM_TYPE}" in
    "Main")
        VERSION_FILE="${ETC_DIR}/version"
        ;;

    "Recovery")
        VERSION_FILE="${ETC_DIR}/recovery-version"
        ;;

    *)
        exit 1
esac

mkdir -p "${ETC_DIR}"

echo "Setting version: ${VERSION}"
[ ! -f "${VERSION_FILE}" ] && echo "${VERSION}" > "${VERSION_FILE}"
