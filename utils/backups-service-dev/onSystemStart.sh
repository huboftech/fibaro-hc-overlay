#!/bin/sh

# check if development backups-service certificate is needed
CERT="/mnt/nvm/backups-service-cacert-dev.pem"
[ -f "${CERT}" ]                                   || exit 0

# get certificate's hash, check if its already installed
HASH=$(openssl x509 -in "${CERT}" -hash -noout)    || exit 1
[ -f "/etc/ssl/certs/${HASH}.0" ]                  && exit 0

# install certificate
mount -o remount,rw /
ln -s "${CERT}" "/etc/ssl/certs/${HASH}.0"
umount /
