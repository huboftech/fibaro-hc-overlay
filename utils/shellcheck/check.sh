#!/bin/bash
ROOT_DIR="${1}"
SHELLCHECK=$(if [ -n "${2}" ]; then echo "${2}"; else which shellcheck; fi)

if [ ! -d "${ROOT_DIR}" ]; then
    echo 'invalid root directory'
    echo 'usage: check.sh <root_dir> [path_to_shellcheck]'
    exit 1
fi

scan_for_scripts() {
	(
	    find "${ROOT_DIR}" -name "*.sh"
	    grep -ril "/bin/sh" "${ROOT_DIR}"
	    test -f "${ROOT_DIR}/opt/fibaro/scripts/config" && echo "${ROOT_DIR}/opt/fibaro/scripts/config"
	) | grep -v "\.git" | grep -v "S51ntp" | sort -u 
}

# ignore false positive
# SC2039 - local is undefined, echo -n etc.
# SC1091 - source not specified
# SC1090 - dynamic source
# SC2034 - unused variables
# SC2154 - variable referenced but not assigned
IGNORED="SC2039,SC1091,SC1090,SC2034,SC2154"

error=0

for file in $(scan_for_scripts) ; do 
	"${SHELLCHECK}" -e "${IGNORED}" "${file}" 1>&2 || error=$?
done

exit "${error}"