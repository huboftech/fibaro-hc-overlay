#!/bin/sh

scriptPath="$( cd "$(dirname ${0})" ; pwd -P )"
overlayPath="${scriptPath}/../.."
platform="${1}"
systemType="${2}"
shellcheckPath="${3}"

coreFiles="core/filesystem"
systemTypeFiles="${systemType}/filesystem"
platformFiles="platforms/${platform}"

error=0

"${scriptPath}"/check.sh "${overlayPath}/${coreFiles}" ${shellcheckPath} || error=$?
"${scriptPath}"/check.sh "${overlayPath}/${systemTypeFiles}" ${shellcheckPath} || error=$?

if [ -d "${overlayPath}/${platformFiles}/${coreFiles}" ]; then
  "${scriptPath}"/check.sh "${overlayPath}/${platformFiles}/${coreFiles}" ${shellcheckPath} || error=$?
fi

if [ -d "${overlayPath}/${platformFiles}/${systemTypeFiles}" ]; then
  "${scriptPath}"/check.sh "${overlayPath}/${platformFiles}/${systemTypeFiles}" ${shellcheckPath} || error=$?
fi

exit "${error}"
