#!/bin/sh

scriptDir="$(dirname ${0})"
shunit2Path="${scriptDir}/external/shunit2/shunit2"
testsPath="${scriptDir}/tests"

getTests()
{
  local directory="${1}"

  if [ -d "${directory}" ] ; then
    echo "$(ls ${directory})"
  fi
}

executeTest()
{
  local test="${1}"
  . "${test}"
  . "${shunit2Path}"
}

runTests()
{
  for test in "$(getTests ${testsPath})" ; do
    echo "Running test ${test}..."
    ( executeTest "${testsPath}/${test}" )
  done
}

if [ ${#} -lt 1 ] ; then
  echo "usage: ${0} <path-to-overlay>"
  exit 1
fi
echo ${0}
OVERLAY_PATH="${1}"

runTests || return $?
