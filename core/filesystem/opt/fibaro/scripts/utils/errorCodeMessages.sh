#!/bin/sh

# add description for more codes if needed
_http_error_code_msg()
{
    local httpCode="${1}"

    case "${httpCode}" in
        400)
            echo "Bad request"
        ;;
        403)
            echo "Forbidden"
        ;;
        404)
            echo "Not found"
        ;;
        405)
            echo "Method not allowed"
        ;;
        500)
            echo "Internal server error"
        ;;
        507)
            echo "Insufficient storage"
        ;;
    esac
}

_curl_error_code_msg()
{
    local curlCode="${1}"

    case "${curlCode}" in
        35)
            echo "CURLE_SSL_CONNECT_ERROR: A problem occurred somewhere in the SSL/TLS handshake. You really want the error buffer and read the message there as it pinpoints the problem slightly more. Could be certificates (file formats, paths, permissions), passwords, and others."
        ;;
        51)
            echo "CURLE_PEER_FAILED_VERIFICATION: The remote server's SSL certificate or SSH md5 fingerprint was deemed not OK. "
        ;;
        60)
            echo "CURLE_SSL_CACERT: Peer certificate cannot be authenticated with known CA certificates."
        ;;
        77)
            echo "CURLE_SSL_CACERT_BADFILE: Problem with reading the SSL CA cert (path? access rights?)"
        ;;
    esac
}

_error_code_msg()
{
    local codeType="${1}"
    local code="${2}"
 
    case "${codeType}" in
        "http")
            echo "${codeType} error code ${code} [$(_http_error_code_msg "${code}")] returned"
            ;;
        "curl")
            echo "${codeType} error code ${code} [$(_curl_error_code_msg "${code}")] returned"
            ;;
        *)
            echo "error code ${code} returned"
            ;;
    esac
}
