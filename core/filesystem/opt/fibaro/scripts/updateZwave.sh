#!/bin/sh
. /opt/fibaro/scripts/config

if [ $# -ne 2 ]; then
	echo "usage: $0 <sdk-version> <zwave-flash-file>"
	return 1 
fi

newVersion=$1
newHexFile=$2

#check Z-Wave frequency
freq=$(getZwaveFrequency)
filename=$(basename "${newHexFile}" ".hex")

sync

if [ "${filename}" != "${freq}" ]; then
	echo "Wrong frequency; \"${freq}\" expected, provided \"${filename}\"" 
	return 2
fi

#stop Zwave process so that it won't interfere with ZwSerial
zwaveKilled=1
isProcessRunning Zwave && killall -9 Zwave || zwaveKilled=0

sdkVersion=$(cat "${currentZwaveVersion}")

if [ "${sdkVersion}" != "${newVersion}" ]; then
	echo "Writing Z-Wave flash memory..."
	if writeZwaveFlash "${newHexFile}"; then 
		echo "${newVersion}" > "${currentZwaveVersion}"
		sync
		echo "Z-Wave flash memory write success."
	else 
		echo "Z-Wave flash memory write failed."
		exit 3
	fi
else
	echo "Z-Wave flash memory is up-to-date"
fi

#restart Zwave if it was killed
if [ "${zwaveKilled}" -eq 1 ]; then
	screen -dmS Zwave /opt/fibaro/Zwave
	echo "Restarting Z-wave..."
fi

sleep 5

return 0
