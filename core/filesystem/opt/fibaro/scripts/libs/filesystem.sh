#!/bin/sh

getAbsolutePath() {
  local path="${1}"

  if [ "${path}" = "" ]; then
    pwd
  elif [ "${path}" != "${path#/}" ]; then
    echo "${path}"
  else
    echo "$(pwd)/${path}"
  fi 
}

getFullDirName () {
  local relativePath;relativePath=$(dirname "$1")
  local dirName;dirName=$(getAbsolutePath "${relativePath}")
  echo "$dirName"
}

isCurrentDir() {
  local directory; directory="$(getAbsolutePath "${1}")"
  [ "${PWD}" = "${directory}" ] && return 0 || return 1
}
#check if path contains current working directory
containsCurrentDir() {
  local directory; directory="$(getAbsolutePath "${1}")"

  if isCurrentDir "${directory}" ; then
    return 1
  fi

  case "${PWD}"/ in
    "${directory}"/*) return 0;;
    *) return 1;;
  esac
}

_checkAndCreateDir() {
  if [ ! -d "${1}" ]; then
      mkdir -p "${1}"
  fi
}

# get directory content except for specified content
get_dir_content_except() {
  local directory; directory="${1:?}"
  shift

  cmd="ls ${directory}"
  for arg in "$@"
  do
    cmd="${cmd} | grep -v -w '${arg}'"
  done

  echo "${cmd}" | sh
}

safe_clean_dir() {
  local directory; directory="$(getAbsolutePath "${1:?}")"

  if [ "${directory}" = '/bin' ] || [ "${directory}" = "/" ]; then
    echo "ERROR: invalid clean directory path: ${directory}"
    return 1
  fi

  if containsCurrentDir "${directory}" ; then
    echo "ERROR: ${directory} contains current working directory (${PWD}). Removing it could have unexpected consequences."
    return 2
  fi

  rm -rf "${directory:?}"/*
  return $?
}

# clean dir except for specified content
safe_clean_dir_except() {
  local directory; directory="$(getAbsolutePath "${1:?}")"

  if [ "${directory}" = '/bin' ] || [ "${directory}" = "/" ]; then
    echo "ERROR: invalid clean directory path: ${directory}"
    return 1
  fi

  # shellcheck disable=SC2068
  content_to_remove="$(get_dir_content_except $@)"

  # check content_to_remove before removing anything
  for d in ${content_to_remove} ; do
    if containsCurrentDir "${directory}/${d}" || isCurrentDir "${directory}/${d}" ; then
      echo "ERROR: ${d} contains/is current working directory (${PWD}). Removing it could have unexpected consequences."
      return 2
    fi
  done

  for content in ${content_to_remove} ; do
    rm -rf "${directory:?}/${content:?}"
  done

  return 0
}

# can also remove non-directory content
safe_remove_dir() {
  local directory; directory="$(getAbsolutePath "${1}")"

  if [ "${directory}" = '/bin' ] || [ "${directory}" = "/" ]; then
    echo "ERROR: invalid remove directory path: ${directory}"
    return 1
  fi

  if containsCurrentDir "${directory}" || isCurrentDir "${directory}" ; then
    echo "ERROR: ${directory} contains/is current working directory (${PWD}). Removing it could have unexpected consequences."
    return 2
  fi

  rm -rf "${directory}"
  return $?
}

safe_move_dir_content() {
  if [ "${1}" != '/bin' ] && [ "${1}" != "" ] && [ "${1}" != '/' ]; then
    mv "${1}"/* "${2}"/
  else
    echo "ERROR: invalid move directory content path: ${1}"
  fi
  return $?
}

# move dir content except for specified content
safe_move_dir_content_except() {
  if [ "${1}" != '/bin' ] && [ "${1}" != "" ] && [ "${1}" != '/' ]; then
    local src="${1}"
    local dst="${2}"

    #remove destination ($2) from argument list
    set -- "${1}" "${@:3}"

    local IFS=$'\n'
    for content in $(get_dir_content_except "$@")
    do
      mv "${src}/${content}" "${dst}"
    done
  else
    echo "ERROR: invalid move directory content path: ${1}"
  fi
  return $?
}
