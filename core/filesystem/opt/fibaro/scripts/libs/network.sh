#!/bin/sh
fileNetSettingsScript="/opt/fibaro/scripts/net.sh"
fileNetworkConf="${userDataPath}/network.conf"
fileNetworkInterfaces="${userDataPath}/network-interfaces"

isDhcpEnabled()
{
  dhcp=$("${fileNetSettingsScript}" get | cut -d '|' -f1)
  [ "${dhcp}" -eq 1 ] && return 0 || return 1
}
