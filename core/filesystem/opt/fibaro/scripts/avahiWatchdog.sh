#!/bin/sh
. /opt/fibaro/scripts/config

mdnsName="$(/bin/hostname).local"

#observation: if mDNS is working properly avahi-resolve returns
#output in format: "<hostname>        <ipv4>", otherwise it's
#"<hostname>        <ipv6>" or an error message

# avahi-resolve output contains multiple consecutive whitespaces
# which make the output incomprehensible to cat function
# proxying the output via echo without quotes solves this issue
#shellcheck disable=SC2005,SC2046
resolveResult=$(echo $(avahi-resolve -n "${mdnsName}") | cut -d " " -f2)
ipAddress=$(${fileNetSettingsScript} get | cut -d "|" -f2)

if [ "${resolveResult}" != "${ipAddress}" ]; then
    avahiService="/etc/init.d/S50avahi-daemon"
    ${avahiService} stop
    ${avahiService} start
fi
