#!/bin/sh

#DESCRIPTION: split STDOUT/STDERR to STDOUT and file
# function reexecute script to capture output
# it also redirect STDERR to STDOUT
# Usage:
# setup_self_logging <tag> [args...]
setup_self_logging() {
    local tag=$1
    shift

    if [ "$SELF_LOGGING" != "1" ]
    then
        # The parent process will enter this branch and set up logging
        log() {
            while IFS= read -r line; do
                echo "${line}" | logger -s -t "${tag}"
            done
        }

        # Create a named piped for logging the child's output
        PIPE=$(mktemp -u).fifo
        mkfifo "${PIPE}"

        # Launch the child process redirected to the named pipe
        # shellcheck disable=SC2086
        SELF_LOGGING=1 sh "${0}" "$@" &> "${PIPE}" &

        # Save PID of child process
        PID=$!

        # Launch tee in a separate process
        #tee ${logFile} <$PIPE &
        log < "${PIPE}"

        # Unlink $PIPE because the parent process no longer needs it
        rm "${PIPE}"

        # Wait for child process running the rest of this script
        wait "${PID}"

        # Return the error code from the child process
        exit $?
    fi
}
