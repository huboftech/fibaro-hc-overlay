#!/bin/sh
. /opt/fibaro/scripts/config

controlLed() {
  if [ "${1}" -eq 0 ] ; then
    led_online "${2}"
    sleep 3
    /opt/fibaro/scripts/checkInternet.sh
  fi
}

if isDhcpEnabled ; then
  ${fileNetSettingsScript} set static 192.168.81.1
  controlLed $? blink_fast
else
  ${fileNetSettingsScript} set dhcp
  controlLed $? blink_slow
fi

exit 0
