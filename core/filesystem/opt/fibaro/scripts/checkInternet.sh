#!/bin/sh
. /opt/fibaro/scripts/config

renewDhcpLease()
{
  if isDhcpEnabled ; then
    udhcpc -n -q
  fi
}

if grep -q "1" /sys/class/net/eth0/carrier; then
  led_ethernet on
  if ping -c 1 www.google.com; then
    echo "1" > "${fileOnline}"
    led_online on
  else
    echo "0" > "${fileOnline}"
    led_online off
    renewDhcpLease
  fi
else
  echo "0" > "${fileOnline}"
  led_online off
  led_ethernet off
fi
