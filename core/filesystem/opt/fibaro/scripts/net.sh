#!/bin/sh
. /opt/fibaro/scripts/config

getInfo() 
{
  ip=$(ifconfig eth0 | grep "inet addr" | tr -s ' ' ':' | cut -d ':' -f 4)
  mask=$(ifconfig eth0 | grep "inet addr" | tr -s ' ' ':' | cut -d ':' -f 8)
  gw=$(route -n | grep ^0.0.0.0 | tr -s ' ' | cut -d ' ' -f 2)
  dns=$(grep "nameserver" /etc/resolv.conf | head -n 1 | cut -d ' ' -f 2)
  dhcp=$(grep -c "iface eth0 inet dhcp" "${fileNetworkInterfaces}")

  echo -ne "$dhcp|$ip|$mask|$gw|$dns"
}

createNetworkConfFileForStaticIp() {
  dhcp="dhcp=0"
  address="address=${1}"
  netmask=""
  gateway=""
  dns=""
  
  if [ $# -ge 2 ] ; then netmask="netmask="${2} ; else netmask="netmask=255.255.255.0" ; fi
  if [ $# -ge 3 ] ; then gateway="gateway="${3} ; else gateway="gateway=${1}" ; fi
  if [ $# -ge 4 ] ; then 
    dns="dns=${4}"
  else 
    dns="dns=$(grep 'nameserver' /etc/resolv.conf | head -n 1 | cut -d ' ' -f 2)"
  fi
  
  { echo "${dhcp}";
    echo "${address}";
    echo "${netmask}";
    echo "${gateway}";
    echo "${dns}"; } > "${fileNetworkConf}"

  sync
}

createNetworkInterfaceFile() {
  dhcp="1"
  address=""
  netmask=""
  gateway=""
  dns=""

  echo -ne "auto lo\\niface lo inet loopback\\nauto eth0\\n" > "${fileNetworkInterfaces}"

  # read information from conf file (if it exists)
  if [ -f "${fileNetworkConf}" ] ; then
    dhcp=$(grep dhcp "${fileNetworkConf}" | awk -F= '{ print $2 }')
    address=$(grep address "${fileNetworkConf}" | awk -F= '{ print $2 }')
    netmask=$(grep netmask "${fileNetworkConf}" | awk -F= '{ print $2 }')
    gateway=$(grep gateway "${fileNetworkConf}" | awk -F= '{ print $2 }')
    dns=$(grep dns "${fileNetworkConf}" | awk -F= '{ print $2 }')
  fi

  # create /etc/network/interfaces & /etc/resolv.conf
  if [ "${dhcp}" = "1" ] ; then
    { echo -ne "auto lo\\niface lo inet loopback\\nauto eth0\\n";
      echo "iface eth0 inet dhcp"; } > "${fileNetworkInterfaces}"
    true > /etc/resolv.conf
  else
    { echo -ne "auto lo\\niface lo inet loopback\\nauto eth0\\n";
    echo "iface eth0 inet static";
    echo "address $address";
    echo "netmask $netmask";
    echo "gateway $gateway"; } > "${fileNetworkInterfaces}"

    if [ "${dns}" = "8.8.8.8" ] ; then
      echo "nameserver 8.8.8.8" > /etc/resolv.conf
    else
      { echo "nameserver $dns";
        echo "nameserver 8.8.8.8"; } > /etc/resolv.conf
    fi
  fi

  sync
}

showHelp()
{
  echo "Usage:
  net.sh get
  net.sh set static IP MASK GW DNS
  net.sh get static IP
  net.sh set dhcp
  net.sh reload"
}
  
restartNetwork()
{
  screen -dmS IFDOWN ifdown -f eth0
  screen -dmS IFUP ifup -f eth0
  sleep 2
}

reload()
{
  if [ -e "${fileNetworkConf}" ] ; then
    . "${fileNetworkConf}"
  
    if [ "${dhcp}" -eq 0 ] ; then
      createNetworkConfFileForStaticIp "${address}" "${netmask}" "${gateway}" "${dns}"
    fi

    createNetworkInterfaceFile
    getInfo
  fi
}

ARGC=$#
  
if [ "$1" = "get" ] ; then
  getInfo
elif [ "$1" = "set" ] ; then
  if [ "$2" = "static" ] ; then
	if [ "${ARGC}" -eq 6 ] ;then
      createNetworkConfFileForStaticIp "$3" "$4" "$5" "$6"
      createNetworkInterfaceFile
      restartNetwork
	elif [ "${ARGC}" -eq 3 ] ; then
      createNetworkConfFileForStaticIp "$3"
      createNetworkInterfaceFile
      restartNetwork
	else
      echo "Invalid argument count for set static: ${ARGC}"
      exit 1
    fi
  elif [ "$2" = "dhcp" ] ; then
    echo "dhcp=1" > "${fileNetworkConf}"
    sync

    createNetworkInterfaceFile
    restartNetwork  
  else
    showHelp
    exit 0
  fi

  sync
  getInfo
elif [ "$1" = "reload" ] ; then
    reload
else
  showHelp
fi

exit 0
