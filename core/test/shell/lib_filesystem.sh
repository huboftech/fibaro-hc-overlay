#!/bin/sh

oneTimeSetUp()
{ 
  . "${OVERLAY_PATH}/opt/fibaro/scripts/libs/filesystem.sh"
}

setUp()
{
  baseDir="$(pwd)"

  #prepare content for tests
  mkdir -p "${baseDir}/foo/bar/baz"
  touch "${baseDir}/foo/bar/file1"
  touch "${baseDir}/foo/bar/file2"
  touch "${baseDir}/foo/bar/file3"
  mkdir -p "${baseDir}/foo/barr/baz"
  touch "${baseDir}/foo/barr/file1"
  touch "${baseDir}/foo/barr/file2"
  touch "${baseDir}/foo/barr/file3"
  mkdir -p "${baseDir}/foo/barrr/baz"
  mkdir -p "${baseDir}/foo/barrrr/baz"
  mkdir -p "${baseDir}/foo/some_prefix_bar/baz"
}

tearDown()
{
  cd "${baseDir}"
  rm -rf "${baseDir}/foo"
}

testGetAbsolutePathFromRelativePath()
{
  local path="foo"
  assertEquals "$(pwd)/${path}" "$(getAbsolutePath ${path})"
}

testGetAbsolutePathFromAbsolutePath()
{
  local path="${baseDir}/foo/bar"
  assertEquals "${path}" "$(getAbsolutePath ${path})"
}

testGetAbsolutePathEmptyPath()
{
  assertEquals "${baseDir}" "$(getAbsolutePath '')"
}

testIsCurrentDir()
{
  local currentPath="$(pwd)"
  isCurrentDir "${currentPath}"
  result=$?

  assertTrue "${currentPath} is not current dir (${currentPath})" "${result}"
}

testIsNotCurrentDir()
{
  local currentPath="$(pwd)"
  local subdir="$(pwd)/foo"
  
  isCurrentDir "${subdir}"
  result=$?

  assertFalse "${subdir} is current dir (${currentPath})" "${result}"
}

testContainsCurrentDir()
{
  local baseDir="$(pwd)"
  local directory="${baseDir}/foo/bar"
  mkdir -p "${directory}"
  cd "${directory}"

  containsCurrentDir "${baseDir}"
  result=$?

  assertTrue "${directory} does not contain ${baseDir}" "${result}"
}

testDoesNotContainCurrentDir()
{
  # case 1 - subdir of current dir
  containsCurrentDir foo/bar
  result=$?
  assertFalse "${result}"
  
  # case 2 - checked path is a substring of currentPath
  cd foo/barr
  containsCurrentDir "${baseDir}/foo/bar"
  result=$?
  assertFalse "${directory} contains ${baseDir}" "${result}"
}

testGetDirContentExcept()
{
  local testedDir="${baseDir}/foo"
  local omittedContent="bar barrr"
  local content="$(get_dir_content_except ${testedDir} bar barrr)"

  local expected="barr barrrr some_prefix_bar "

  assertEquals "${expected}" "$(echo ${content} | tr '\n' ' ')" # TODO: fix trailing space issue in this comparison
}

testSafeRemoveDir()
{
  local dirToRemove="${baseDir}/foo/bar"

  safe_remove_dir "${dirToRemove}"
  result=$?
  assertTrue "safe_remove_dir failed: ${result}" "${result}"

  assertFalse "Directory ${dirToRemove} still exists" "[ -d ${dirToRemove} ]"
}

testSafeRemoveCurrentDir()
{
  local dirToRemove="${baseDir}/foo/bar"
  cd "${dirToRemove}"

  safe_remove_dir "${dirToRemove}"
  result=$?
  assertFalse "safe_remove_dir was executed" "${result}"

  assertTrue "Directory ${dirToRemove} has been removed" "[ -d ${dirToRemove} ]"
}

testSafeRemoveContainingDir()
{
  local dirToRemove="${baseDir}/foo/bar"
  cd "${dirToRemove}/baz"

  safe_remove_dir "${dirToRemove}"
  result=$?
  assertFalse "safe_remove_dir was executed" "${result}"

  assertTrue "Directory ${dirToRemove} has been removed" "[ -d ${dirToRemove} ]"
}

testSafeCleanDir()
{
  local dirToClean="${baseDir}/foo/bar"

  safe_clean_dir "${dirToClean}"
  result=$?
  assertTrue "safe_clean_dir failed: ${result}" "${result}"

  local dirContent="$(ls -A ${dirToClean})"
  assertTrue "Directory ${dirToClean} is not empty:\n${dirContent}" "[ -z ${dirContent} ]"
}

testSafeCleanCurrentDir()
{
  # current dir can be cleaned as opposed to being removed
  local dirToClean="${baseDir}/foo/bar"
  cd "${dirToClean}"

  safe_clean_dir "${dirToClean}"
  result=$?
  assertTrue "safe_clean_dir failed: ${result}" "${result}"

  local dirContent="$(ls -A ${dirToClean})"
  assertTrue "Directory ${dirToClean} is not empty:\n${dirContent}" "[ -z ${dirContent} ]"
}

testSafeCleanContainingDir()
{
  # if cleaned directory contains current dir partial cleaning should be prevented
  local dirToClean="${baseDir}/foo/bar"
  cd "${dirToClean}/baz"

  safe_clean_dir "${dirToClean}"
  result=$?
  assertFalse "safe_clean_dir has been executed" "${result}"

  local dirContent="$(ls -A ${dirToClean})"
  assertFalse "Directory ${dirToClean} is empty" "[ -z ${dirContent} ]"
}

testSafeCleanDirExcept()
{
  local dirToClean="${baseDir}/foo/bar"
  notToBeCleaned="baz file2"

  safe_clean_dir_except "${dirToClean}" ${notToBeCleaned}
  result=$?
  assertTrue "safe_clean_dir failed: ${result}" "${result}"

  local dirContent="$(ls -A ${dirToClean} | tr '\n' ' ')"
  assertEquals "${notToBeCleaned} " "${dirContent}" # TODO: fix trailing space issue in this comparison
}

testSafeCleanContainingDirExcept()
{
  # current dir is a part of content to be removed; cleaning should not happen
  local dirToClean="${baseDir}/foo"
  notToBeCleaned="barr"

  cd "${baseDir}/foo/bar"
  local initDirContent="$(ls -AR ${dirToClean})"

  safe_clean_dir_except "${dirToClean}" ${notToBeCleaned}
  result=$?
  assertFalse "safe_clean_dir_except has been executed" "${result}"

  local finalDirContent="$(ls -AR ${dirToClean})"
  assertEquals "${initDirContent}" "${finalDirContent}"
}

testSafeCleanContainingDirExcept2()
{
  # current dir is a subdir of dir to be cleaned, but not a part of content to be cleaned
  # cleaning should happen normally
  local dirToClean="${baseDir}/foo"
  notToBeCleaned="barr barrr"

  cd "${baseDir}/foo/barr"

  safe_clean_dir_except "${dirToClean}" ${notToBeCleaned}
  result=$?
  assertTrue "safe_clean_dir failed: ${result}" "${result}"

  local dirContent="$(ls -A ${dirToClean} | tr '\n' ' ')"
  assertEquals "${notToBeCleaned} " "${dirContent}" # TODO: fix trailing space issue in this comparison
}
