# HC Overlay

Overlay for HC system.

## Overlay structure

Files are put into the following hierarchy (based on their affiliation to different platforms and system types):
- core - all platforms and systems,
- main - Main system of any platform,
- recovery - Recovery system of any platform,
- platform - platform-specific; those files are further divided into their own core/main/recovery categories.

### Files

At each level of the above hierarchy files are placed in 1 of 4 directories. This determines how they would be installed in the output built for given platform and system type:
- filesystem - any file placed here is meant to be installed as-is (copied into the output). Paths relative to the *filesystem* directory indicates direct paths on the built system,
- src - source files for any packages meant to be compiled for target system with their respective installation directives,
- external - describes required external packages (e.g. Zwave or HCServer binaries) with their respective installation directives,
- scripts - scripts to be executed during installation process.

## Installation

HC Overlay is using CMake as its build system. To build an overlay for given platform and system type execute the following commands in Unix shell:
```bash
mkdir -p <build-dir>
cd <build-dir>
cmake -DCMAKE_TOOLCHAIN_FILE=<path-to-toolchain-file> -DCMAKE_INSTALL_PREFIX=<output-dir> -DPLATFORM=<platform-tag> -DSYSTEM_TYPE=<system-type> -DEXTERNAL_PACKAGE_DIR=<external-package-dir> <path-to-hc-overlay>
make
make install
```
CMake parameters explained:
- path-to-toolchain-file - path to cross-compilation toolchain file for target system,
- output-dir - path where output overlay would be created,
- platform-tag - indicates target platform (e.g. HC2, HCLITE),
- system-type - target system type (Main or Recovery),
- external-package-dir - path to directory containing all required external packages,
- path-to-hc-overlay - path to this repository.

### Specifying build info

HC Overlay allows to specify build info by adding the following flags to the above cmake command:
- -DBUILD_TAG=<tag> - tag to be put into the /etc/build-version file (or /etc/recovery-build-version for Recovery system),
- -DFIBARO_VERSION=<version> - Fibaro software version (/etc/version or /etc/recovery-version for Recovery system),
- -DFIBARO_VERSION_TYPE=<version-type> - alpha/beta/stable (/etc/versionType or /etc/recovery-versionType for Recovery system.

## ShellCheck

Scripts contained within *filesystem* directory ought to be checked using ShellCheck, a static analysis tool for shell scripts (check https://github.com/koalaman/shellcheck for more information and installation guide).
To perform script validation for given platform and system type execute the following script
```bash
sh utils/shellcheck/checkOverlay.sh <platform> <system-type>
```
e.g.
```bash
sh utils/shellcheck/checkOverlay.sh hc2 main
```
Optionally a path to shellcheck binary can be specified, e.g.
```bash
sh utils/shellcheck/checkOverlay.sh hc2 main ~/.cabal/bin/shellcheck
```

## Testing

Tests for features from any given hierarchy level can be placed in respective *test* directory (e.g. core/test for core functionalities).
During overlay installation process all tests found for system being built are gathered in a directory named after path specified by CMAKE_INSTALL_PREFIX with a *-test* suffix (e.g. /tmp/hc2-output-test if overlay has been installed in /tmp/hc2-output).
To facilitate building for testing the overlay can be built without providing external packages. This can be done by replacing EXTERNAL_PACKAGE_DIR CMake flag with WITHOUT_EXTERNAL_PACKAGES flag, e.g. command from Installation paragraph would transform into:
```bash
cmake -DCMAKE_TOOLCHAIN_FILE=<path-to-toolchain-file> -DCMAKE_INSTALL_PREFIX=<output-dir> -DPLATFORM=<platform-tag> -DSYSTEM_TYPE=<system-type> -DWITHOUT_EXTERNAL_PACKAGES=1 <path-to-hc-overlay>
```
### Shell script testing
HC Overlay uses shunit2 (https://github.com/kward/shunit2) for shell scripts unit testing. Unit tests should be placed in *test/shell* directory and will be installed into *<output-dir>-test/shell* directory. All tests can be than executed through:
```bash
<output-dir>-test/shell/runTests.sh <output-dir>
```
