cmake_minimum_required(VERSION 2.8)

message("Adding recovery system files")

add_subdirectory(filesystem)
add_subdirectory(src)
add_subdirectory(scripts)
