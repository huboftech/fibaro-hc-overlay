#!/bin/sh

ETC_DIR="${1}/etc"
BUILD_VERSION="${2}"
SYSTEM_TYPE="${3}"

case "${SYSTEM_TYPE}" in
    "Main")
        BUILD_VERSION_FILE="${ETC_DIR}/build-version"
        ;;

    "Recovery")
        BUILD_VERSION_FILE="${ETC_DIR}/recovery-build-version"
        ;;

    *)
        exit 1
esac

mkdir -p "${ETC_DIR}"

echo "Setting build-version: ${BUILD_VERSION}"
[ ! -f "${BUILD_VERSION_FILE}" ] && echo "${BUILD_VERSION}" > "${BUILD_VERSION_FILE}"
