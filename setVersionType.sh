#!/bin/sh

ETC_DIR="${1}/etc"
VERSION_TYPE="${2}"
SYSTEM_TYPE="${3}"

case "${SYSTEM_TYPE}" in
    "Main")
        VERSION_TYPE_FILE="${ETC_DIR}/versionType"
        ;;

    "Recovery")
        VERSION_TYPE_FILE="${ETC_DIR}/recovery-versionType"
        ;;

    *)
        exit 1
esac

mkdir -p "${ETC_DIR}"

echo "Setting versionType: ${VERSION_TYPE}"
[ ! -f "${VERSION_TYPE_FILE}" ] && echo "${VERSION_TYPE}" > "${VERSION_TYPE_FILE}"
