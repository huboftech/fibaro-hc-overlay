cmake_minimum_required(VERSION 2.8)

message("Adding platform-specific files")
add_subdirectory(${PLATFORM_DIR_NAME})
