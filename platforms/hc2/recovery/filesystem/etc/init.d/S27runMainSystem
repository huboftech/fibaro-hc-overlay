#!/bin/sh
#
# Start the system
#
. /opt/fibaro/scripts/deviceUtils.sh
. /opt/fibaro/scripts/config
. /opt/fibaro/scripts/logging.sh

setup_self_logging "fibaro.scripts.$(basename "${0}")" "$@"

shouldStartSystem()
{
	GPIOUSBDevice="/dev/controller"
	systemType=$(cat "${hwDataDir}"/systemType)
	echo "System type - ${systemType}"

	if  [ -f "${recoveryFlag}" ]; then
		echo "${recoveryFlag} found. Removing it."
		rm "${recoveryFlag}"
		return 1
	elif [ -e "${GPIOUSBDevice}" ]; then
		# GPIOServer setup
		mkdir -p "/tmp/fibaro" # directory for GPIOServer socket
		# 0 - main system
		# 1 - recovery system
		
		/opt/fibaro/GPIOServer -s
		echo "GPIOServer -s; status: $? (0 - main system, 1 - recovery system)"
		
		isRecoveryButtonPressed=$(getSystemState)

		if [ "${isRecoveryButtonPressed}" -eq 1 ]; then
			echo "Recovery button was pressed. Resetting recovery flag."
			clearRecoveryFlag
			return 1
		else
			echo "Recovery button was not pressed."
			return 0
		fi
	elif [ "${systemType}" = "virtual" ]; then
		echo "This is a virtual machine without /dev/controller - its probably run on a PC and GPIO won't work (hardcoded). Starting main system..."
		sleep 2
		return 0
	else
		return 0
	fi
}

case "$1" in
  start)
	mkdir -p "/tmp/fibaro" # For GPIO sockets

	if [ -f "${updateFlag}" ] ;	then
		echo "Staying in recovery for an update."
	elif shouldStartSystem ; then
		echo "Booting main system"
		rm -f "${systemFlagUpdating}"
		echo "UUID=${systemUUID}"
		kexec -l "${systemDir}"/boot/bzImage --initrd="${systemDir}"/boot/initramfs --append="root=UUID=${systemUUID} rootdelay=2"
		kexec -e
	else
		echo "Staying in recovery."
		stopBootWatchdog
		stopRebootAnimation
		echo "Starting Fibaro Finder..."
		screen -dmS FibaroFinder /opt/fibaro/FibaroFinderServer
	fi
	;;
  stop)
	echo -n "S27runMainSystem - stop called"
	startRebootAnimation
	startBootWatchdog
	killall -w -9 FibaroFinderServer
	;;
  restart|reload)
	"$0" stop
	"$0" start
	;;
  *)
	echo "Usage: $0 {start|stop|restart}"
	exit 1
esac

exit $?