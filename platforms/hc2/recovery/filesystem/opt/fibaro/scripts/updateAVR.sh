#!/bin/sh
if [ -f "${FIBARO_CONFIG_FILE}" ]; then 
    . "${FIBARO_CONFIG_FILE}"
else 
    . /opt/fibaro/scripts/config 
fi

if [ $# -ne 2 ] ; then
    echo "usage: $0 <avr-soft-version> <avr-hex-file>"
    return 1
fi

newVersion="${1}"
hexFile="${2}"
currentVersion=$(getAVRVersion)

if [ "${currentVersion}" != "${newVersion}" ]; then
    echo "Stopping GPIO Server ..."
    stopGpioServer
    sync
    sleep 5s

    echo "Flashing AVR chip ..."
    if programAVRChip "${hexFile}"; then
        echo "System state: $(getSystemState)"
        echo "${newVersion}" > "${fileControllerVersion}"
    else
        echo "System state: $(getSystemState)"
        echo "Flashing AVR chip failed."; exit 2
    fi

    sleep 5s
    sync

    echo "Starting GPIO Server ..."
    startGpioServer
else
    echo "AVR chip is up-to-date"
fi

return 0