#!/bin/sh
getRootDevice()
{
	LABEL="$(sed -ne 's/.*LABEL=\([^ ]*\).*/\1/p' < /proc/cmdline)"  # Get rid of everything before LABEL= and after the first space
	UUID="$(sed -ne 's/.*UUID=\([^ ]*\).*/\1/p' < /proc/cmdline )"  # Get rid of everything before UUID= and after the first space

	if [ "${LABEL}" != "" ] ; then
		blkid | grep "${LABEL}" | cut -c 1-8
	elif [ "${UUID}" != "" ] ; then
		blkid | grep "${UUID}" | cut -c 1-8
	else
		echo "error: could not deduce root device from ${cmdline}"
		exit 1
	fi
}
getUUID()
{
	partitionList=$(blkid | grep "${1}")
	UUID="${partitionList##*"UUID=\""}" # Get rid of everything before uuid="
	UUID="${UUID%%\"*}"                 # Get rid of everything after "
	echo "${UUID}"
}

flashSystemPartition()
{
	local logsPath="/mnt/event_data/log"

	rootFSImage=$1
	if [ ! -r "${rootFSImage}" ] ; then
		echo "FS image ${rootFSImage} not found or unreadable."
		exit 1
	fi

	umount -f "${systemDir}"
	echo "umount -f ${systemDir}; state: $?"
	sync

	echo "Writing system image to disk ${systemPartition}..."
	dd if="${rootFSImage}" of="${systemPartition}"
	echo "dd if=${rootFSImage} of=${systemPartition}; state: $?"
	sync

	echo "Resizing filesystem to actual size"
	tune2fs "${systemPartition}" -L "${systemLabel}" -U "${systemUUID}"
	echo "tune2fs ${systemPartition} -L ${systemLabel} -U ${systemUUID}; state: $?"

	e2fsck -f -v -y "${systemPartition}"
	echo "e2fsck -f -v -y ${systemPartition}; state: $?"

	resize2fs "${systemPartition}"
	echo "resize2fs ${systemPartition}; state: $?"

	sync
	partprobe

	# Mounting so that the next script can start the system already
	mount "${systemPartition}" "${systemDir}"
	echo "mount ${systemPartition} ${systemDir}; state: $?"

	# checking filesystem
	# shellcheck disable=SC2010,SC2126
	ls -laR / 2>/dev/null | grep '?' > "${logsPath}/checking_filesystem.log"
	# shellcheck disable=SC2010,SC2126
	ls -laR / 2>/dev/null | grep '?'| wc -l >> "${logsPath}/checking_filesystem.log"
	# shellcheck disable=SC2010,SC2126
	echo "Defective files: $(ls -laR / 2>/dev/null | grep '?'| wc -l)"
}

rootDevice=$(getRootDevice)

# Partition labels
systemLabel="SystemFS"
recoveryLabel="RecoveryFS"
downloadedLabel="Downloaded"
nvmDataLabel="NVMData"
hwDataLabel="HwData"
swapLabel="Swap"
userDataLabel="UserData"

# Partition numbers
recoveryPartition="${rootDevice}1"
systemPartition="${rootDevice}2"
downloadedPartition="${rootDevice}3"
nvmDataPartition="${rootDevice}5"
hwDataPartition="${rootDevice}6"
swapPartition="${rootDevice}7"
userDataPartition="${rootDevice}8"

# Partition UUIDs
systemUUID="ff76e33e-c562-4ab5-87f3-5c5f0f4c0c86"
recoveryUUID="b21a51d1-7963-41ac-9724-3040961e176a"
downloadedUUID="0389e371-a737-44dc-bd7e-c4add77b6e66"
nvmDataUUID="b162bcde-76e2-43cd-8a11-a0e55d2dace4"
hwDataUUID="2c85e78e-9fee-4dd2-9143-72021f261560"
swapUUID="28eb7f87-1673-4065-9149-45900eed7a7f"
userDataUUID="d1794d9e-d84d-467e-b06b-6cf4cb756e92"

# Partition mount directories - if you want to change this on main system, edit /etc/fstab
systemDir="/mnt/main_system"
downloadedDir="/mnt/download"
nvmDataDir="/mnt/nvm"
hwDataDir="/mnt/hw_data"
userDataDir="/mnt/user_data"

# Boot flags
recoveryFlag="${nvmDataDir}/stayInRecovery"
updateFlag="${nvmDataDir}/updateSoftware"
migrationFlag="/opt/fibaro/migrateToNHC"
formatFlag="${nvmDataDir}/formatSLC"
factoryResetFlag="${nvmDataDir}/factoryReset"

# Update related things
mainRootFSImage="${downloadedDir}/rootfs_main.ext2"
mainRootVersion="${downloadedDir}/info"
