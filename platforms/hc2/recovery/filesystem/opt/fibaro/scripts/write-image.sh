#!/bin/sh

. /opt/fibaro/scripts/config
. /opt/fibaro/scripts/deviceUtils.sh
. /opt/fibaro/system/status-utils
. /opt/fibaro/scripts/logging.sh

setup_self_logging "fibaro.scripts.$(basename "${0}")" "$@"

error_and_exit () {
  echo "$@"; exit 1
}

set_recovery_update_type() {
  echo "recovery" > "${SYSTEM_OS_FILE_PATH}"
}

#
# prepare headers for curl commands
#
serialNumber=$(cat "${fileSerial}")
headers="--header X-Fibaro-Serial-Number:${serialNumber} --header X-Fibaro-Soft-Version:recovery-v2"

#
# functions
#

remove_extract_dir() {
  echo "Cleaning up extract dir..."
  safe_remove_dir "${extractDir}"
}

clean_download_dir() {
  echo "Cleaning up download dir"
  safe_clean_dir "${downloadDataPath}"
}

decompress_image() {
    set_status_system_updating_decompressing_image
    echo "Decompressing image archive..."
    7zr x "${patchFile}" -o"${extractDir}" -y
    return $?
}

download_image() {
    led_update blink_fast
    set_status_system_updating_downloading_update
    echo "Downloading image file..."
    /opt/fibaro/scripts/update.sh --download stable
    local ret; ret=$?
    led_update off
    return "${ret}"
}

verify_update_image() {
    [ $# -eq 0 ] && set_status_system_updating_verifying_image

    if [ -f "${flagIgnoreSignature}" ]; then
        echo "Ignoring signature"
        return 0
    fi

    echo "Verifying image file..."

    # If some required files are missing and there is a compressed patch present, decompress it and try again
    if [ ! -f "${updateImageRootfs}" ] || [ ! -f "${updateImageVersion}" ] || [ ! -f "${updateImageSignature}" ]; then
        if [ -f "${patchFile}" ] ; then
            decompress_image || {
              set_status_system_updating_error_image_corrupted
            }
        fi
    fi

    # Verify if all required files are in place
    if [ ! -f "${updateImageRootfs}" ] || [ ! -f "${updateImageVersion}" ] || [ ! -f "${updateImageSignature}" ]; then
        echo "Required files are missing!"
        safe_clean_dir "${downloadDataPath}"
        return 1
    fi

    # Verify signature with public key
    cat "${updateImageRootfs}" "${updateImageVersion}" | openssl dgst -sha512 | awk -F"= " '{ print $2 }' > /tmp/hash
    openssl rsautl -verify -inkey "${fileFibaroPublicKey}" -keyform PEM -pubin -in "${updateImageSignature}" > /tmp/verified

    diff -s /tmp/hash /tmp/verified
    local valid=$?

    rm /tmp/hash /tmp/verified

    # If verification was unsuccessful, clean up and exit
    if [ "${valid}" -ne 0 ] ; then
        echo "Provided signature is invalid!"
        clean_download_dir
        return 1
    fi

    echo "Copying version info to download partition"
    cp "${updateImageVersion}" "${downloadDataPath}/version"
    cp "${updateImageVersionType}" "${downloadDataPath}/versionType"

    echo "Verification succeeded"
    return 0
}

write_update_image() {
    led_recovery blink_fast
      set_status_system_updating_writing_image
      echo "Writing system image..."
      flashSystemPartition "${updateImageRootfs}" || { led_recovery on; return 1; }
      echo "Checking AVR chip..."
      ${updateAvrScript} "$(getUpdateAvrVersion)" "${updateAvrFile}" || { echo "Checking AVR Error!"; led_recovery on; return $?; }
      echo "Checking Z-Wave chip..."
      ${updateZwaveScript} "$(getUpdateZwaveVersion)" "$(updateZwaveFile)" || { echo "Checking Z-Wave Error!"; led_recovery on; return $?; }
      rm -f "${updateFlag}"

    led_recovery on
    return 0
}

clear_user_data() {
    set_status_system_updating_restoring_defaults
    echo "Clearing user data..."
    rm -f "${fileBTKey}"
    rm -rf /mnt/user_data/* > /dev/null || return 1
    erase_zwave_eeprom > /dev/null 2>&1 || return 2

    # Read-only file system, password cannot be modified
    # local pass; pass=$(getPass)
    # echo -e "${pass}\n${pass}" | passwd root
    # cp  /etc/passwd /mnt/main_system/etc
    # cp  /etc/passwd- /mnt/main_system/etc
    # cp  /etc/shadow /mnt/main_system/etc

    touch "${initializeFlag}"

    return 0
}

factory_reset_prepare_reports() {
  [ -f "${factoryResetFlag}" ] || return 0
  rm "${factoryResetFlag}"

  #FIXME: migration.log is for early development purpose only - to be removed once all layers and their API's are ready (cloud, SecServer, etc)
  MIGRATION_LOG="${nvmDataDir}/migration.log"

  echo "Factory reset triggered remotely" >> "${MIGRATION_LOG}"
  if [ -f "${fileMigrationTicket}" ]; then
     echo "Using migration ticket" >> "${MIGRATION_LOG}"
     MIGRATION_TICKET_DATA=$(cat "${fileMigrationTicket}")
     rm "${fileMigrationTicket}"
  else
     echo "No migration ticket provided" >> "${MIGRATION_LOG}"
  fi

  return 0
}

factory_reset_report_progress() {
  [ -z "${MIGRATION_TICKET_DATA}" ] && return
  echo "Factory reset progress: ${1}" >> "${MIGRATION_LOG}"

  curl -retry 1 -F "PK_AccessPoint=$(getSerial)" \
                -F "HW_Key=$(getHWKey)" \
                -F "Progress=${1}" \
                "${remoteFactoryResetProgressScript}" >> "${MIGRATION_LOG}"
}

factory_reset_report_finished() {
  [ -z "${MIGRATION_TICKET_DATA}" ] && return
  echo "Factory reset finished for migration" >> "${MIGRATION_LOG}"
  echo "Sending ticket:${MIGRATION_TICKET_DATA}"

  curl -retry 3 -F "PK_AccessPoint=$(getSerial)" \
                -F "HW_Key=$(getHWKey)" \
                -F "Ticket=${MIGRATION_TICKET_DATA}" \
                "${remoteFactoryResetFinishedScript}" >> "${MIGRATION_LOG}"
}

factory_reset_report_error() {
  [ -z "${MIGRATION_TICKET_DATA}" ] && return
  echo "Factory reset error: ${1}" >> "${MIGRATION_LOG}"

  curl -retry 3 -F "PK_AccessPoint=$(getSerial)" \
                -F "HW_Key=$(getHWKey)" \
                -F "Error=${1}" \
                "${remoteFactoryResetErrorScript}" >> "${MIGRATION_LOG}"
}

#
# update from uploaded file
#
start_upload() {
    echo "Starting upload..."

    decompress_image || {
      set_status_system_updating_error_image_corrupted
      clean_download_dir
      error_and_exit "decompressing image error"
    }

    verify_update_image || {
      set_status_system_updating_error_image_corrupted
      clean_download_dir
      error_and_exit "image verifying failed"
    }

    write_update_image || {
      set_status_system_updating_error_while_flashing_image
      error_and_exit "writing update image failed"
    }

    echo "Done"
}


# start_repair is only available  from recovery os
# function set os file to recovery
start_repair() {
    echo "Starting repair..."

    decompress_image || {
      set_status_system_updating_error_image_corrupted
      clean_download_dir
      error_and_exit "decompressing image error"
    }
    
    if ! verify_update_image "without_status_change" ; then
      download_image || {
          set_status_system_updating_error_while_downloading_patch
          error_and_exit "download image error"
      }

      decompress_image || {
        set_status_system_updating_error_image_corrupted
        error_and_exit "decompressing image error"
      }

      verify_update_image || {
        set_status_system_updating_error_image_corrupted
        error_and_exit "image verifying failed"
      }
    fi

    write_update_image || {
      set_status_system_updating_error_while_flashing_image
      error_and_exit "writing update image failed"
    }

    echo "Done"
}

start_factory_default() {
    echo "Starting factory default settings restoration..."
    factory_reset_prepare_reports || {
      factory_reset_report_error "factory_reset_prepare"
      error_and_exit "migration ticket read failed"
    }
    factory_reset_report_progress 5

    if [ -f "${flagIgnoreSignature}" ]; then
      download_image || {
          set_status_system_updating_error_while_downloading_patch
          factory_reset_report_error "download_image"
          error_and_exit "download image error"
      }
      factory_reset_report_progress 30

      decompress_image || {
        set_status_system_updating_error_image_corrupted
        factory_reset_report_error "decompress_image"
        error_and_exit "decompressing image error"
      }

    elif ! verify_update_image "without_status_change" ; then
      download_image || {
          set_status_system_updating_error_while_downloading_patch
          factory_reset_report_error "download_image"
          error_and_exit "download image error"
      }
      factory_reset_report_progress 15

      decompress_image || {
        set_status_system_updating_error_image_corrupted
        factory_reset_report_error "decompress_image"
        error_and_exit "decompressing image error"
      }
      factory_reset_report_progress 25

      verify_update_image || {
        set_status_system_updating_error_image_corrupted
        factory_reset_report_error "verify_update_image"
        error_and_exit "image verifying failed"
      }
    fi
    factory_reset_report_progress 50

    write_update_image || {
      set_status_system_updating_error_while_flashing_image
      factory_reset_report_error "write_update_image"
      error_and_exit "writing update image failed"
    }
    factory_reset_report_progress 80

    clear_user_data || {
      factory_reset_report_error "clear_user_data"
      error_and_exit "clearing user data"
    }
    factory_reset_report_progress 95

    factory_reset_report_finished
    echo "Done"
}

start_update() {
    echo "Starting update..."

    decompress_image || {
      set_status_system_updating_error_image_corrupted
      error_and_exit "decompressing image error"
    }

    verify_update_image || {
      set_status_system_updating_error_image_corrupted
      error_and_exit "image verifying failed"
    }

    write_update_image || {
      set_status_system_updating_error_while_flashing_image
      error_and_exit "writing update image failed"
    }

    remove_extract_dir
    echo "Done"
}

#
# main
#
clear files

case "${1}" in
    "--upload")
        set_recovery_update_type
        start_upload
        ;;
    "--repair")
        set_recovery_update_type
        start_repair
        ;;
    "--factory-default")
        set_recovery_update_type
        start_factory_default
        ;;
    "--update")
        start_update
        ;;
    *)
        echo "Wrong arguments"
        echo 'usage:
        --upload
        --repair
        --factory-default
        --update
        '
        exit 1
esac

# All current progress and status must be cleared, otherwise it will be
# continued whenever recovery system starts.
set_status_idle
/usr/bin/systemReboot
