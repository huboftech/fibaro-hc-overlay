#!/bin/sh
#
# Created by a.przybecki@fibargroup.com on 23.09.16.
#

. /opt/fibaro/scripts/config
. /opt/fibaro/scripts/logging.sh
. /opt/fibaro/scripts/utils/errorCodeMessages.sh
. /opt/fibaro/system/status-utils


setup_self_logging "/mnt/event_data/$(basename "${0}").log" "$@"

#error handling variables
errorCodeType=""
errorCode=0

_usage()
{
echo '
=======================================
     FIBARO UPDATE
=======================================

Specify usage: update --[cmd] [opt [arg]]
cmd:
 --check           Downloads Manifest and checks available versions.
 --download [opt]  Downloads specified software version. Available
                   options are [stable, beta, alpha, <version>].
 --cancel-download [opt] Cancel already started download
 --install         Installs downloaded software.
 --auto [opt]      Automatically downloads and installs software.
                   Available options are [stable, beta, alpha, <version>]
 --clear           Removes all downloaded files.
 --help            Displays this usage message.

opt:
 --without-backup  Installs package without creating backup.
 --uri [opt]       Overrides default software source.

 example:
 update.sh --download stable
 update.sh --download 4.101
 update.sh --download stable --uri http://google.com
 update.sh --auto beta
 update.sh --auto 4.101
 update.sh --auto 4.101 --without-backup
 update.sh --install --without-backup'
}

timeout=20

prefix=""
updateType="stable"
cmd="check"
without_backup="false"

DOWNLOAD_PIDFILE="/var/run/updateDownload.pid"
DOWNLOAD_INFOFILE="/var/run/updateDownload.info"

set_normal_update_type() {
  echo "normal" > "${SYSTEM_OS_FILE_PATH}"
}

# split stdin by lines and call command for each line
# usage: for_each_line command
for_each_line()
{
    local cmd=${1}
    while read -r line; do
        ${cmd} "${line}"
    done < /dev/stdin
}

_clearUpdateFlag()
{
  [ ! -f "${updateFlag}" ] || rm -f "${updateFlag}"
}

_clearDownloadWithoutLocalBackup()
{
  echo "Clearing download partition..."

  local dirToClean="${downloadDataPath}"
  local notToBeCleaned="backups"

  safe_clean_dir_except "${dirToClean}" "${notToBeCleaned}"

  _clearUpdateFlag
  echo "Done."
}

_error()
{
  echo -ne "ERROR: $1 \n" 1>&2
  exit "${2}"
}

# special function to show progress of update

_startUpdateGotoRecovery()
{
  sync
  echo "--- Rebooting..."
  reboot
}

_setFlagUpdateStatusRecovery()
{
  echo "--- Setting update flag status recovery"
  touch "${systemFlagUpdating}"
}

_setUpdateFlag()
{
  echo "--- Setting update availability flag for recovery system"
  touch "$updateFlag"
}

validate_checksum()
{
  # Check checksum
  checksumCalculated=$(sha512sum "${patchFile}" | tr -s ' ' | cut -d ' ' -f 1)
  checksumFromFile=$(cat "${checksumFile}")

  if [ "${checksumCalculated}" != "${checksumFromFile}" ] || [ "${checksumCalculated}" = "" ]
  then
      set_status_system_updating_error_invalid_checksum
      _error "Wrong checksum! Aborting." 8
  fi

  echo "Checksum verified."
  return 0
}

_get()
{
    # [URI] [OUT_FILE] [PROGRESS_ENABLED]
    local uri=${1}
    local out=${2}
    local send_progress=${3}

    echo "${updateType}:${updateVersion}" > "${DOWNLOAD_INFOFILE}"
    /opt/fibaro/scripts/download.sh "${uri}" "${out}" "${send_progress}" "${updateType}" "${updateVersion}"
    local ret=$?
    rm -f "${DOWNLOAD_INFOFILE}"
    return "${ret}"
}

_autobackup()
{
  if [ $without_backup = "false" ]; then
    echo "--- Creating Auto Backup"
    # createBackup shutdown fibaro services,
    # statuses are updated through stages file in createBackup.sh
    createBackup.sh --remote "Auto backup $hcVersion" --autobackup || _error "Auto Backup don't create"
  fi
}

_downloadManifest()
{
  local hcSerialNumber; hcSerialNumber=$(getSerial)
  local hcVersion; hcVersion=$(getVersion)
  local hcDefaultLanguage; hcDefaultLanguage=$(getDefaultLanguage)
  local headers="--header X-Fibaro-Soft-Version:${hcVersion} \
  --header X-Fibaro-Serial-Number:${hcSerialNumber} \
  --header X-Fibaro-Default-Language:${hcDefaultLanguage}"

  set_status_downloading_progress 0 "${updateType}" "${updateVersion}"
  echo "Downloading manifest file ${urlSoft}/${infoUrl} ..."

  if ! _get "${headers} ${urlSoft}/${infoUrl}" "${infoFile}" "0" || [ ! -s "${infoFile}" ] ; then
	set_status_downloading_manifest_error "${updateType}" "${updateVersion}"
     _error "error while downloading info file from server. Aborting. " 5
  fi
}

###### Check update available
_getVersion()
{
  version=$(grep -w -m1 "$1" "${infoFile}" | cut -d '=' -f 2)
  if [ -z "${updateVersion}" ]
  then
    updateVersion="${version}"
  fi
  # Get prefix for
  prefix=$(grep -w -m1 "${1}_prefix" "${infoFile}" | cut -d '=' -f 2)
  description=$(grep -w -m1 "${1}_description" "${infoFile}" | cut -d '=' -f 2)
  echo "version: ${version} type: ${1} prefix: ${prefix} description: ${description}"
}

_download()
{
  _clearDownloadWithoutLocalBackup

  patchURL="${urlSoft}/${updateVersion}/${patchName}"
  checksumURL="${urlSoft}/${updateVersion}/${checksumName}"

  if [ ! -z "${prefix}" ] ; then
    patchURL="${urlSoft}/${prefix}/${updateVersion}/${patchName}"
    checksumURL="${urlSoft}/${prefix}/${updateVersion}/${checksumName}"
  fi
  echo "Downloading patch - ${patchURL}  ..."

  if !  _get "${patchURL}" "${patchFile}" "1"; then
     set_status_downloading_patch_error "${updateType}" "${updateVersion}"
     _error "error while downloading patch file from server. Aborting." 6
  fi

  echo "Downloading checksum..."

  if !  _get "${checksumURL}" "${checksumFile}" "0"; then
  # Usage udpateProgress: [version] [type] [status] [progress]
     set_status_downloading_checksum_error "${updateType}" "${updateVersion}"
     _error "error while downloading checksum file from server. Aborting." 7
  fi
  # Usage udpateProgress: [version] [type] [status] [progress]
  set_status_downloading_done "${updateType}" "${updateVersion}"
  echo "${updateType}" > "${fileLocalVersionType}"
  echo "${updateVersion}" > "${fileLocalVersion}"
}

_install()
{
  # Usage udpateProgress: [version] [type] [status] [progress]
  # checksum patch
  # create backup
  validate_checksum && _autobackup && {
    _setFlagUpdateStatusRecovery
    _setUpdateFlag
    set_normal_update_type
    _startUpdateGotoRecovery
  }
}


cancel_download()
{
    # if background downloading process is not running ignore this call
    if [ -f "${DOWNLOAD_INFOFILE}" ]; then
      killall download.sh
      killall curl
      local info; info="$(cat "${DOWNLOAD_INFOFILE}")"
      set_status_downloading_canceled "${info%%:*}" "${info##*:}"
    fi
}


is_one_of() {
    echo "${2}" | grep -qw "${1}"
}

if [ "$#" -lt 1 ] ; then
  echo "Error: Insufficient number of parameters. Expected at least 1, given $#" 1>&2
  _usage
  exit 1
fi

while [ "$1" != "" ]; do
  case $1 in
    --cancel-download)
        shift
        cmd="cancel-download"
        ;;
    --download )
        cmd="download"
        shift
        if is_one_of "$1" "stable,beta,alpha" ; then
          updateType="$1"
        elif echo "$1" | grep -i -E '^\b[0-9][.][0-9][0-9][0-9]\b' >/dev/null 2>&1 ; then
          updateVersion=$1
        fi
        ;;

    --install )
        cmd="install"
        ;;

    --auto )
        cmd="auto"
        shift
        if is_one_of "$1" "stable,beta,alpha" ; then
          updateType="$1"
        elif echo "$1" | grep -i -E '^\b[0-9][.][0-9][0-9][0-9]\b' >/dev/null 2>&1 ; then
          updateVersion=$1
        fi
        ;;

    --without-backup )
        without_backup="true"
        ;;

    --uri )
        shift
        urlSoft=$1
        echo "Changing URI to: $urlSoft"
        ;;

    --check )
        _downloadManifest
        _getVersion "stable"
        _getVersion "beta"
        _getVersion "alpha"
        exit 0
        ;;

    --clear )
        _clearDownloadWithoutLocalBackup
        exit 0
        ;;

    --help )
        _usage
        exit 0
        ;;

    * ) _usage
        _error "Invalid parameter: $1" 1
  esac
  shift
done

case "${cmd}" in
    "download")
        _downloadManifest
        _getVersion "${updateType}"
        _download
        exit 0
        ;;
    "auto")
        _downloadManifest
        _getVersion "${updateType}"
        _download
        _install
        exit 0
        ;;
    "install")
        _install
        exit 0
        ;;
    "cancel-download")
        cancel_download
        ;;
    * )  _usage
        _error "Invalid arguments" 1
esac
