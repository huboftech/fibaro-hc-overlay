#!/bin/sh
#
# Created by a.przybecki@fibargroup.com on 23.09.16.
#

# USAGE: [version] [type] [status] [progress]
. /opt/fibaro/scripts/config

_updateProgress(){

echo "version: ${1}"
echo "type: ${2}"
echo "status: ${3}"
echo "progress: ${4}"

if [ "${3}" = "${statusDownloading}" ]
then
    # 0 - 100 download file 0-50 progress for recovery download file
    progressRecover=$(awk "BEGIN {print int($4*0.5)}")
    progress "${progressRecover}" "downloadingImage"
fi
}
