<?php

require_once("../../authorize.php");
require_once("../../dbSession.php");
require_once("../../instances.php");

function authorize()
{
   if (!isAuthorized() &&  !isAuthorizedFibaroAuth(array(role::USER, role::INSTALLER)))
   {
      sendUnauthorized();
      exit(0);
   }
}

require("httpStatuses.php");

// Main function

if(getNumberOfInstances(__FILE__) > 1)
{
    setStatusTooManyRequests();
    die();
}

$requestBody = file_get_contents('php://input');
$requestMethod = $_SERVER['REQUEST_METHOD'];

if ($requestMethod == "POST") {
  authorize();

  if (getNumberOfInstances('{screen} SCREEN -dmS REBOOT') > 0)
  {
      setStatusTooManyRequests();
      return;
  }

  $data = json_decode($requestBody);
  $cmd = 'factoryReset.sh "' . (isset($data->ticket) ? $data->ticket : '') . '"';
  exec($cmd, $output, $ret_val);

  if ($ret_val) {
      setStatusBadRequest();
      return;
  }

  setStatusAccepted();
  exec('screen -dmS REBOOT /usr/sbin/rebootToRecovery');
}
else
{
    //TODO check return code in HCServer
    setStatusMethodNotAllowed();
}

?>
