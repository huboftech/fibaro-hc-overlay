<?php
if (file_exists("/tmp/led_status"))
{
  header('Content-Type: application/json');
  echo file_get_contents("/tmp/led_status");
}
else
{
  header('HTTP/1.0 404 Not Found');
  header('Content-Length 0');
}
?>