#!/bin/sh

. /opt/fibaro/scripts/config

HW_KEY=$(getHWKey)
HDD_SERIAL=$(getHDDserial)
HC_SERIAL=$(getSerial)

test_backups_key() {
  local KEY="${1}"
  local TEST_STR="test string"
  # shellcheck disable=SC2155
  local ENCRYPTED=$(echo "${TEST_STR}"  | backupTool encrypt_backup -h "${HW_KEY}" -s "${HDD_SERIAL}" -k "${KEY}") || return 1
  # shellcheck disable=SC2155
  local DECRYPTED=$(echo "${ENCRYPTED}" | backupTool decrypt_backup -h "${HW_KEY}" -s "${HDD_SERIAL}" -k "${KEY}") || return 1
  [ "${DECRYPTED}" == "${TEST_STR}" ]                                                                              || return 1
  return 0
}

if [ -f "${fileBTKey}" ]; then
  echo "Backups-key ${fileBTKey} exists - veryfing..."
  KEY=$(cat "${fileBTKey}")
  test_backups_key "${KEY}" && echo "Backups-key verified OK" && exit 0

  echo "Backups-key is invalid - removing"
  rm "${fileBTKey}"
else
  echo "Backups-key not found"
fi

echo "Requesting backups-key from cloud service..."
KEY=$(curl --retry 1 -f -F "PK_AccessPoint=${HC_SERIAL}" -F "HW_Key=${HW_KEY}" "${remoteGetBackupKeyScript}") || {
  echo "Failed to retrieve backups-key - curl error:${?}"
  exit 1
}

echo "Received backups-key - veryfing..."
test_backups_key "${KEY}" || {
  echo "Received backups-key is invalid"
  exit 1
}

echo "Backups-key is OK - saving"
echo "${KEY}" > "${fileBTKey}"
exit 0
