<?php
require_once("/var/www/dbSession.php");

$GLOBALS["pendriveScript"] = "/opt/fibaro/scripts/libs/recoveryPendrive.sh";

function getPendriveBackupList()
{
    exec($GLOBALS["pendriveScript"] . " --check", $output, $retval);

    if ($retval == 0)
    {
        // get pendrive backup list from db
        $dbConnection = createSession();
        $query = "SELECT Id, DirectoryPath, Date, Devices, Rooms, Scenes, Description, Version, Compatible FROM Backups ORDER BY Date DESC, Id DESC";
        $queryResult = $dbConnection->query($query);

        $backupList = array();
        while ($entry = $queryResult->fetchArray())
        {
            $backupEntry = array(
                    'id' => $entry["Id"],
                    'timestamp' => $entry["Date"],
                    'compatible' => false,
                    'automatic' => false, // automatic backups are not stored on recovery pendrive
                    'devices' => $entry["Devices"],
                    'rooms' => $entry["Rooms"],
                    'scenes' => $entry["Scenes"],
                    'description' => $entry["Description"],
                    'softVersion' => $entry["Version"],
                    'size' => 0 // this field is left for compatibility 
                );

            array_push($backupList, $backupEntry);
        }

        $dbConnection->close();

        $used = exec("/opt/fibaro/scripts/libs/recoveryPendrive.sh --usedSpace");
        $total = exec("/opt/fibaro/scripts/libs/recoveryPendrive.sh --totalSpace");
        
        $percentage = number_format($used/$total * 100, 0) . "%";

        $result = array(
            'totalSpace' => $total,
            'usedSpace' => $used,
            'usedSpacePercentage' => $percentage,
            'type' => "pendrive",
            'backups' => $backupList 
        );

        return json_encode($result);
    }
}

?>
