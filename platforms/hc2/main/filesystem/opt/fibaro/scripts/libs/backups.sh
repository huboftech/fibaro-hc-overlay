#!/bin/sh

tmpBackupPath="/tmp/backups"
backupPath="/mnt/download/backups"
backupFilename="backup.7z"

remoteBackupServer="https://backups.fibaro.com"
remoteGetBackupsListScript="${remoteBackupServer}/getBackupsList.php"
remoteSaveBackupScript="${remoteBackupServer}/saveBackup.php"
remoteGetBackupScript="${remoteBackupServer}/getBackup.php"
remoteDeleteBackupScript="${remoteBackupServer}/deleteBackup.php"
remoteGetBackupKeyScript="${remoteBackupServer}/getBackupsKey.php"

recoveryPendriveDir="/mnt/recovery_pendrive"
pendriveBackupDir="${recoveryPendriveDir}/backups"

pendriveBackupsScriptDir="/opt/fibaro/scripts/libs"
pendriveScript="${pendriveBackupsScriptDir}/recoveryPendrive.sh"
pendriveGetBackupsList="${pendriveBackupsScriptDir}/getBackupsList.php"
pendriveSynchronizeBackups="${pendriveBackupsScriptDir}/synchronizeBackups.sh"

restoreUserData() {
  safe_clean_dir "${userDataPath}" || return 1
  safe_move_dir_content "${tmpBackupPath}" "${userDataPath}" || return 2
  safe_remove_dir "${tmpBackupPath}" || return 3
}

_startServicesIfManualBackup()
{
	if [ "${autoBackup}" != "true" ]; then
		set_status_starting_services
		${fibaroStart} start
	fi
}

_createBackupFile()
{
    ### zwave dump ###
    echo "Backing up Z-Wave..."
    set_status_backup_create_zwave_dump
    readZwaveEeprom "${userDataPath}/${zwaveFile}"
    zwaveCode=${?}

    if [ ${zwaveCode} -ne 0 ] ; then
        set_status_backup_create_error_while_dumping_zwave
        _endScript 2
    fi

    echo "Backing up user data..."
    set_status_backup_create_user_data
    cd "${userDataPath}" || { set_status_backup_create_error_while_backing_up_user_data; _endScript 3; }
    rm -rf "${tmpBackupPath}"
    tar -cf - ./* | 7zr a -m0=lzma2 -mx "${tmpBackupPath}/${backupFilename}" || { set_status_backup_create_error_while_backing_up_user_data; _endScript 4; }

    echo "Backup file size: $(du -h "${tmpBackupPath}/${backupFilename}" | awk '{print $1}')"
}

_encryptBackup()
{
    /opt/fibaro/scripts/checkBackupsKey.sh || {
        echo "BTKey not exist"
        set_status_backup_create_error_encryption_failed
        sleep 5
        _startServices
        _endScript 2
    }

    if [ -f "${tmpBackupPath}/${backupFilename}" ]; then
        mv "${tmpBackupPath}/${backupFilename}" "${tmpBackupPath}/tmpBackup.7z"
    fi

    errorCode=$(backupTool encrypt_backup -h "$(getHWKey)" -s "$(getHDDserial)" -k "$(cat "${fileBTKey}")" -i "${tmpBackupPath}/tmpBackup.7z" -o "${tmpBackupPath}/${backupFilename}")

    if ! ${errorCode}; then
        echo "Encryption error: ${errorCode}"
        set_status_backup_create_error_encryption_failed
        sleep 5
        _startServices
        _endScript 3
    fi

    safe_remove_dir "${tmpBackupPath}/tmpBackup.7z"
    echo "Backup file size after encryption: $(du -h "${tmpBackupPath}/${backupFilename}" | awk '{print $1}')"    
}

_decryptBackup()
{
  /opt/fibaro/scripts/checkBackupsKey.sh || {
    echo "BTKey not exist"
    set_status_backup_restore_error_decryption_failed
    return 1
  }

  if [ -f "${tmpBackupPath}/${backupFilename}" ]; then
    mv "${tmpBackupPath}/${backupFilename}" "${tmpBackupPath}/tmpBackup.7z"
  fi

  errorCode=$(backupTool decrypt_backup -h "$(getHWKey)" -s "$(getHDDserial)" -k "$(cat "${fileBTKey}")" -i "${tmpBackupPath}/tmpBackup.7z" -o "${tmpBackupPath}/${backupFilename}")

  if ! ${errorCode}; then
    echo "Decryption error: ${errorCode}"
    set_status_backup_restore_error_decryption_failed
    return 1
  fi

  safe_remove_dir "${tmpBackupPath}/tmpBackup.7z"
  echo "Backup file size after encryption: $(du -h "${tmpBackupPath}/${backupFilename}" | awk '{print $1}')"    
}

_deletePendriveBackup()
{
    local backupID; backupID=${1}

    if [ -n "${backupID}" ]; then
        echo "Removing pendrive backup ${backupID}..."

        ${pendriveScript} --check || {
            echo "Pendrive is not connected"
            exit 2
        }

        DirectoryPath=$(sqlite3 "${configDbPath}" "SELECT DirectoryPath FROM Backups WHERE Id=${backupID}")

        if [ ! -z "${DirectoryPath}" ]; then
            sqlite3 "${configDbPath}" "DELETE FROM Backups WHERE Id=${backupID}"
            echo "Backup ${backupID} has been successfully removed from database"

            "${pendriveScript}" --mount

            if [ -d "${pendriveBackupDir}/${DirectoryPath}" ]; then
                rm -rf "${pendriveBackupDir:?}/${DirectoryPath:?}"
            fi

            "${pendriveScript}" --umount

            echo "Backup ${backupID} has been successfully removed from filesystem"
        else
            echo "Backup ${backupID} has not been found in database"
            exit 3
        fi

    else
        _error "Invalid backupId: ${backupID}"
        exit 2
    fi
}
