#!/bin/sh

. /opt/fibaro/scripts/config

sqlExec "DELETE FROM Backups"

if ! "${pendriveScript}" --check ; then
    exit 1
fi

"${pendriveScript}" --mount

if [ ! -d "${pendriveBackupDir}" ]; then
    "${pendriveScript}" --umount
    exit 2
fi

for dir in "${pendriveBackupDir}"/* ; do

    #for every directory
    if [ ! -d "${dir}" ]; then
        continue
    fi

    infoFile="${dir}/info"

    if [ ! -f "${infoFile}" ] ; then
        echo "${infoFile} is missing"
        break
    fi

    # get information about backup
    devices=$(grep "^devices" "${infoFile}" | cut -d '=' -f 2-)
    rooms=$(grep "^rooms" "${infoFile}" | cut -d '=' -f 2-)
    scenes=$(grep "^scenes" "${infoFile}" | cut -d '=' -f 2-)
    description=$(grep "^description" "${infoFile}" | cut -d '=' -f 2-)
    timestamp=$(grep "^timestamp" "${infoFile}" | cut -d '=' -f 2-)
    version=$(grep "^version" "${infoFile}" | cut -d '=' -f 2-)

    if [ -z "${timestamp}" ]; then
        continue    
    fi

    # for HOx flways 0
    compatible=0    

    folderName=$(basename "${dir}")

    # add information to database
    sqlExec "INSERT INTO Backups(DirectoryPath, Date, Devices, Rooms, Scenes, Description, Version, Compatible) VALUES ('${folderName}', '${timestamp}', '${devices}', '${rooms}', '${scenes}', '${description}', '${version}', '${compatible}')"

done

"${pendriveScript}" --umount
