#!/bin/sh

. /opt/fibaro/scripts/config

recoveryPendriveUUID="D31B-A75A"

getPartitionDevice()
{
	blkid | grep "${1}" | cut -d ":" -f1
}

isRecoveryPendriveConnected()
{
    if [ -n "$(getPartitionDevice "${recoveryPendriveUUID}")" ] ; then
        echo "Recovery Pendrive is connected"
        return 0
    else
        echo "Recovery Pendrive is disconnected"
        return 1
    fi
}

mountRecoveryPendrive()
{
    local device; device=$(getPartitionDevice "${recoveryPendriveUUID}")

    if [ -n "${device}" ] ; then
        mount -t vfat "${device}" "${recoveryPendriveDir}"
        return 0
    fi

    return 1
}

umountRecoveryPendrive()
{
    umount "${recoveryPendriveDir}"
    return $?
}

getTotalSpace()
{
    local directory; directory="${1}"
    df -B1 "${directory}" | tail -1 | awk '{print $2}'
}

getUsedSpace()
{
    local directory; directory="${1}"
    df -B1 "${directory}" | tail -1 | awk '{print $3}'
}

getAvailableSpace()
{
    local directory; directory="${1}"
    df -B1 "${directory}" | tail -1 | awk '{print $4}'
}

getUsedPercentage()
{
    local directory; directory="${1}"
    df "${directory}" | tail -1 | awk '{print $5}'
}

cmd="${1}"

case "${cmd}" in
  "--check")
    isRecoveryPendriveConnected
    ;;

  "--mount")
    mountRecoveryPendrive
    ;;

  "--umount")
    umountRecoveryPendrive
    ;;

  "--totalSpace")
    getTotalSpace "${recoveryPendriveDir}"
    ;;

  "--usedSpace")
    getUsedSpace "${recoveryPendriveDir}"
    ;;

  "--availableSpace")
    getAvailableSpace "${recoveryPendriveDir}"
    ;;

  "--usedPercentage")
    getUsedPercentage "${recoveryPendriveDir}"
    ;;

  *)
    echo "Invalid command ${1}"
    exit 2
    ;;
esac

exit $?