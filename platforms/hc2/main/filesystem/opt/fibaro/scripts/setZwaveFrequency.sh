#!/bin/sh

freq=$1

validateFrequency()
{
	f=$1

	[ "$f" = EU ] || \
    [ "$f" = US ] || \
	[ "$f" = ANZ ] || \
    [ "$f" = RU ] || \
    [ "$f" = MY ] || \
    [ "$f" = IN ] || \
    [ "$f" = IL ] || \
    [ "$f" = CN ] || \
    [ "$f" = HK ] || \
    [ "$f" = JP ] || \
    [ "$f" = KR ]
}

if validateFrequency "${freq}"; then
	echo "$1" > /mnt/event_data/frequency
else
	echo "${freq} is not a valid Z-Wave frequency"
    exit 1
fi
