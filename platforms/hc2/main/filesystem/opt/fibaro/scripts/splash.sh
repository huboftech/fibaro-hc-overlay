#!/bin/sh

. /opt/fibaro/scripts/config

splash='**************************************************************************
*           _______ __                  ______                           *
*          / ____(_) /_  ____ ______   / ____/________  __  ______       *
*         / /_  / / __ \/ __ `/ ___/  / / __/ ___/ __ \/ / / / __ \      *
*        / __/ / / /_/ / /_/ / /     / /_/ / /  / /_/ / /_/ / /_/ /      *
*       /_/   /_/_.___/\__,_/_/      \____/_/   \____/\__,_/ .___/       *
*                                                         /_/            *
*                                                                        *
**************************************************************************'

ip_addr=$(ifconfig eth0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')
mac_addr=$(ifconfig eth0 | awk '/HWaddr/ {print $NF}')
serial=$(cat "${fileSerial}")
version=$(cat "${fileVersion}")
versionType=$(cat "${fileVersionType}")

RSAKey="Valid"
if ! openssl rsa -inform PEM -in "${fileRSAKey}" -noout > /dev/null 2>&1 ; then
	RSAKey="Invalid"
fi

remoteAccess="Disabled"
if [ "$(pidof autossh)" ] ; then
	remoteAccess="Running"
fi

clear
echo -e "${splash}
IP Address: \\t${ip_addr}
MAC Address: \\t${mac_addr}
Serial: \\t${serial}
Version: \\t${version} ${versionType}
RSA Key: \\t${RSAKey}
Remote Access: \\t${remoteAccess}"