#!/bin/sh

APP_NAME="voip_manager"
SIP_CONF_FILE="/etc/asterisk/sip.conf"
EXTEN_CONF_FILE="/etc/asterisk/extensions.conf"

ALL_USERS_EXTEN="exten => everyone,1,Dial("

removeQuotes()
{
temp="${1%\"}"
temp="${temp#\"}"
echo "$temp"
}

addSipUser ()
{
   sipUserID="$1"
   sipUserPassword="$2"
   sipDisplayName="$3"

   echo "
[$sipUserID]
type=friend
host=dynamic
defaultuser=${sipUserID}
secret=${sipUserPassword}
callerid=${sipDisplayName}
context=default" >> $SIP_CONF_FILE
}

showHelp ()
{
  echo "
DESCRIPTION: Reload VOIP configuration according to entries in database.

USAGE: $APP_NAME reload

EXIT CODES:
  0 - successfull execution
  1 - help message
  2 - must have root priviliges
"
  exit 1
}

#########################
# MAIN                  #
#########################

if [ ! $# -eq 1 ] || [ "$1" != "reload" ]
then
  showHelp
fi


# sip.conf header
echo "
[general]
context=default
allowoverlap=no

udpbindaddr=0.0.0.0

tcpenable=yes
tcpbindaddr=0.0.0.0

transport=udp,tcp
srvlookup=yes

; Codec negotiation
disallow=all
allow=ulaw
allow=alaw
allow=gsm
allow=h264

videosupport=yes

directmedia=no
port=5060
" > $SIP_CONF_FILE

# get devices id list
echo "Searching for devices ..."

# search for all voip devices (16 - HC_user, 19 - helios_IP, 20 - helios_user, 22 - mobileDevice)
query='SELECT id FROM NEW_Device WHERE id IN (SELECT Device_Id FROM NEW_Interface WHERE InterfaceName="voip")'

for dev in $(sqlite3 /mnt/user_data/db "${query}")
do
  sipUserName=$(sqlite3 /mnt/user_data/db "SELECT Value FROM NEW_Property WHERE Device_Id = $dev AND Name = 'sipUserID'")
  sipUserPassword=$(sqlite3 /mnt/user_data/db "SELECT Value FROM NEW_Property WHERE Device_Id = $dev AND Name = 'sipUserPassword'")
  sipDisplayName=$(sqlite3 /mnt/user_data/db "SELECT Value FROM NEW_Property WHERE Device_Id = $dev AND Name = 'sipDisplayName'")

  deviceType=$(sqlite3 /mnt/user_data/db "SELECT Device_Type FROM NEW_Device WHERE Id = $dev AND Device_Type = 'com.fibaro.fibaroIntercom'")

  #string values in db have quotes that needs to be removed
  sipUserPassword=$(removeQuotes "${sipUserPassword}")
  sipDisplayName=$(removeQuotes "${sipDisplayName}")
  sipUserName=$(removeQuotes "${sipUserName}")


  # if userName is not empty
  if [ "${sipUserName}" != "" ]
  then

    echo "VOIP client found:"
    echo "  deviceID = ${dev}"
    echo "  userID = ${sipUserName}"
    echo "  userPassword = ${sipUserPassword}"
    echo "  displayName = ${sipDisplayName} "
    echo "  deviceType = ${deviceType}"

    addSipUser "${sipUserName}" "${sipUserPassword}" "${sipDisplayName}"


    if [ "${sipUserName}" != "1223334444" ] && [ "${deviceType}" = "" ]
    then
      ALL_USERS_EXTEN="${ALL_USERS_EXTEN}SIP/$sipUserName&"
    fi
  fi

  echo -e "\\n\\n"
done

# extensions.conf
ALL_USERS_EXTEN="${ALL_USERS_EXTEN%?},60)"

echo "
[default]
exten => _HELIOS-X!,1,AGI(intercom.php);

exten => _X!,1,Dial(SIP/\${EXTEN},,m);

${ALL_USERS_EXTEN};

" > $EXTEN_CONF_FILE

# reload asterisk
echo "Reloading asterisk configuration ..."
asterisk -r -x reload

exit 0
