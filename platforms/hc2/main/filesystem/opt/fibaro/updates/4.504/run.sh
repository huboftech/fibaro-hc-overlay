#!/bin/sh

. /opt/fibaro/scripts/config

fixRegion()
{
  /etc/init.d/fibaro/GPIOServer start
  mount -o remount,rw /mnt/hw_data/
  
  region=$1
  echo "Fix region to: $region"

  

  if GPIOClient writeZwave "/opt/fibaro/ZWFrequency/$region.hex"; then
    echo "Update filesystem"
    echo "$region" > /mnt/hw_data/frequency
    sqlite3 /mnt/user_data/db  "update hc_data set frequency='$region'"
  fi

  /etc/init.d/fibaro/GPIOServer stop
}

DIR=$(getFullDirName "${0}")

if [ -d "${DIR}" ]; then
  sqlExecByDir "${DIR}"/sql
fi

SERIAL=$(getSerial)
region=$(grep "${SERIAL}" < "/opt/fibaro/updates/4.504/regions.csv" | cut -d ',' -f 2)

if [ ! -z "${region}" ]; then
  hcRegion=$(cat /mnt/hw_data/frequency)
  if [ "${region}" != "${hcRegion}" ]; then
    fixRegion "${region}"
  fi 
fi