-- api class implementation
api = {}

api.get = function(uri)
	uri = "/api" .. uri
	local http = Net.FHttp("127.0.0.1", 11111)
	local result, status, errorCode = http:GET(uri)
	if result ~= "" then
		return json.decode(result)
	else
		return ""
	end
end
	

api.put = function(uri, data)
	uri = "/api" .. uri
	local http = Net.FHttp("127.0.0.1", 11111)
	local jsonData = json.encode(data)
	local result, status, errorCode = http:PUT(uri, jsonData)
	if result ~= "" then
		return json.decode(result)
	else
		return ""
	end
end


api.post = function(uri, data)
	uri = "/api" .. uri
	local http = Net.FHttp("127.0.0.1", 11111)
	local jsonData = json.encode(data)
	local result, status, errorCode = http:POST(uri, jsonData)
	if result ~= "" then
		return json.decode(result)
	else
		return ""
	end
end




--LuaEnvironment writes debugs to console (print(txt)), but PluginManager sends them to HC2 - overwrite it
fibaro.debug = function(self, text)
	fibaro:call(fibaro:getSelfId(), "addDebugMessage", _elementID_, text, "debug")
end




--PluginManager engine has different implementation of sleep - overwrite it
fibaro.sleep = function(self, ms)
	fibaro_:sleep(ms)
end


--Some wrappers to expose residual part of PluginManager fibaro API
fibaro.HttpGET = function(self, uri)
	return fibaro_:HttpGET(uri)
end

fibaro.HttpPOST = function(self, uri, payload)
	return fibaro_:HttpPOST(uri, payload)
end

fibaro.HttpPUT = function(self, uri, payload)
	return fibaro_:HttpPUT(uri, payload)
end

fibaro.HttpDELETE = function(self, uri)
	return fibaro_:HttpDELETE(uri)
end


fibaro.getSelfId = function(self)
	return fibaro_:getSelfId()
end

fibaro.log = function(self, text)
	fibaro:call(fibaro:getSelfId(), "log", text)
end

fibaro.logTransferFailed = function(self)
	fibaro:log("ZWave_Send_Failed")
end

fibaro.logTransferOK = function(self)
	fibaro:log("Transfer_was_OK")
end




