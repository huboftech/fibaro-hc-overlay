#!/bin/bash
FILENAME=$1
SCHEMAS_DIRECTORY=/opt/fibaro/schemas

die()
{
    echo "$1"
    exit 1
}

die_on_error()
{
    # shellcheck disable=SC2181
    if [ $? -ne 0 ]; then
        echo -n "$1"
        if [ ! -z "$TEMP_DIR" ]; then
            rm -rf "${TEMP_DIR}" &> /dev/null
        fi
        rm "${FILENAME}" &> /dev/null
        exit 1
    fi
}

if [ -z "$FILENAME" ]; then
    die "Archive file name not specified"
fi

# Testowanie archiwum
unzip -qq -t "${FILENAME}" &> /dev/null
die_on_error "Archive verification failed"

# Tworzenie tymczasowego katalogu
TEMP_DIR=$(mktemp -dq)
die_on_error "Cannot create temporary directory"

# Wypakowanie pliku do tymczasowego katalogu
unzip -qq "${FILENAME}" -d "${TEMP_DIR}" &> /dev/null
die_on_error "Archive decompression failed"

# Zmiana praw - upewniamy sie, ze pliki i katalogi maja prawidlowe prawa dostepu
chmod 755 -R "${TEMP_DIR}" &> /dev/null
die_on_error "Incorrect plugin permissions"

# Weryfikacja schemy pliku manifest.xml
xmllint --noout --schema "${SCHEMAS_DIRECTORY}/manifest_schema.xsd" "${TEMP_DIR}/manifest.xml" &> /dev/null
die_on_error "Manifest validation error"

# Pozyskanie identyfikatora wtyczki z pliku manifest.xml
PLUGIN_ID=$(xmllint --xpath '/plugin/identifier/text()' "${TEMP_DIR}/manifest.xml")
CATEGORY=$(xmllint --xpath '/plugin/category/text()' "${TEMP_DIR}/manifest.xml")
die_on_error "Manifest parse failure"
if [ -z "$PLUGIN_ID" ]; then
    die "Incorrect plugin identifier"
fi

# Zamiana pliku manifest.xml na plik description.xml
mv --force "${TEMP_DIR}/manifest.xml" "${TEMP_DIR}/var/www/plugins/${PLUGIN_ID}/xml/description.xml" 2>/dev/null &> /dev/null
die_on_error "Cannot create description file"

# Weryfikacja schemy pliku config.xml i view.xml
for file in $TEMP_DIR/var/www/plugins/*
do
    xmllint --noout --schema "${SCHEMAS_DIRECTORY}/config_schema.xsd" "${TEMP_DIR}/var/www/plugins/$file/xml/config.xml" &> /dev/null
    die_on_error "Configuration XML ($file) validation error"   

    xmllint --noout --schema "${SCHEMAS_DIRECTORY}/view_schema.xsd" "${TEMP_DIR}/var/www/plugins/$file/xml/view.xml" &> /dev/null
    die_on_error "View XML ($file) validation error"
done

# Weryfikacja schemy pliku typu
for file in $TEMP_DIR/var/www/plugins/*
do
    xmllint --noout --schema "${SCHEMAS_DIRECTORY}/type_schema.xsd" "${TEMP_DIR}/opt/fibaro/types/$file.xml" &> /dev/null
    die_on_error "Type XML ($file) validation error"
done

# Kopiowanie pliku z katalogu tymczasowego do docelowego katalogu wtyczki
rsync -q -r "${TEMP_DIR}/opt/fibaro/plugins/" /opt/fibaro/plugins/ &> /dev/null
die_on_error "Cannot copy plugin files"
rsync -q -r "${TEMP_DIR}/opt/fibaro/types/" /opt/fibaro/types/ &> /dev/null
die_on_error "Cannot copy type files"
rsync -q -r "${TEMP_DIR}/var/www/plugins/" /var/www/plugins/ &> /dev/null
die_on_error "Cannot copy asset files"

rm -rf "${TEMP_DIR}" &> /dev/null
rm "${FILENAME}" &> /dev/null

echo -n "${PLUGIN_ID}|${CATEGORY}"
exit 0
