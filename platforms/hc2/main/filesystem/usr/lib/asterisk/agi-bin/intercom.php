#!/usr/bin/php -q
<?
require('php-agi/phpagi.php');

$db = new SQLite3("/opt/fibaro/db");

function executeSQL($db, $query)
{
    $result = $db->query($query);

    if ($row = $result->fetchArray(SQLITE3_NUM))
        return $row[0];
    else
        die ("Error while executing sql query: $query");
}

// ---------------------------------------------------------------------------------------------------------------------------------

// create AGI object
$agi = new AGI();

// get extension from asterisk
$extension = $agi->request["agi_extension"];

// parse helios_user id
$heliosUserID = split("-", $extension);
$heliosUserID = $heliosUserID[1];

// hry helios_user userName
$heliosUserName = executeSQL($db, "SELECT Value FROM NEW_Property WHERE Device_Id = $heliosUserID AND Name = 'userName'");

// get callReceivers (deviceID list)
$callReceivers = executeSQL($db, "SELECT Value FROM NEW_Property WHERE Device_Id = $heliosUserID AND Name = 'callReceivers'");

// get sipUserID list (deviceID -> sipUserID)
$result = $db->query("SELECT Value FROM NEW_Property WHERE Device_Id IN ($callReceivers) AND Name = 'sipUserID'");
$i = 0;

while ($row = $result->fetchArray(SQLITE3_NUM))
        $sipUserID[$i++] = "SIP/" . $row[0];

if (sizeof($sipUserID) == 0)
die ('sipUserID list is empty');

// create DIAL command for asterisk
$dialCmd = "DIAL " . implode("&", $sipUserID) . ",,m";

// overwrite SIP display name and dial numbers
$agi->set_callerid($heliosUserName);
$agi->exec($dialCmd);
?>
