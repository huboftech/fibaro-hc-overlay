#!/usr/bin/php -q
<?
	$fh = fopen('/var/run/lock/asteriskEventsController', 'w');
	if (flock($fh, LOCK_EX | LOCK_NB)) {
		echo 'File locked' . "\n";
	}
	else {
		die('File already locked by another process.' . "\n");
	}

	error_reporting(E_ERROR | E_PARSE);



	require("php-agi/phpagi.php");



	function eventCallback($eventName, $params)
	{
		if($eventName == "bridge")
		{
			if(!$callerSipUserId = getChannelOwner($params["Channel1"]))
				$callerSipUserId = $params["Channel1"];

			if(!$calleeSipUserId = getChannelOwner($params["Channel2"]))
				$calleeSipUserId = $params["Channel2"];

			if($params["CallerID2"] != $calleeSipUserId) //$params["CallerID2"] != $calleeSipUserId means this bridge event was preceded by call to group All Viop Users 
			{	
				$data = array(
				  "data"	=> array(
				  	"callerSipUserId"  => $callerSipUserId,
					 "calleeSipUserId" => $calleeSipUserId
				  	)
				);

				postRequest($eventName, $data);
			}
		}
		elseif($eventName == "shutdown")
		{
			exit;
		}
	}

	function getChannelOwner($string)
 	{
	    $pattern = "/\/(.*?)\-/";
	    preg_match($pattern, $string, $matches);
	    return $matches[1];
 	}

 	function postRequest($eventName, $data)
 	{
	 	$url = "http://127.0.0.1:11111/api/voip/events/".$eventName;

	 	$options = array(
			"http" => array(
		    	"method"  => "POST",
		    	"content" => json_encode($data),
		    	"header"  => "Content-Type: application/json\r\n".
		                     "Accept: application/json\r\n"
		 	)
		);

		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		$response = json_decode($result);
	}

	$agi = new AGI_AsteriskManager();

	if ($agi->connect())
	{
		echo "Connected\n";

		$agi->add_event_handler("bridge", "eventCallback");
		$agi->add_event_handler("shutdown", "eventCallback");
		$agi->wait_response();
	}
	else
	{
		echo "Connection error\n";
	}	
?>