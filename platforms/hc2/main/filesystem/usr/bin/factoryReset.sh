#!/bin/sh

. /opt/fibaro/scripts/config
. /opt/fibaro/scripts/logging.sh

setup_self_logging "fibaro.scripts.$(basename "${0}")" "$@"

TICKET_ENC="${1}"
if [ -z "${TICKET_ENC}" ] ; then
  echo "Triggering factory reset - no migration ticket provided"
  touch "${factoryResetFlag}"
  exit 0
fi

HW_KEY=$(getHWKey)
HDD_SERIAL=$(getHDDserial)
[ -z "${HW_KEY}" ]     && error "Failed to read HWkey"
[ -z "${HDD_SERIAL}" ] && error "Failed to read HDDserial"

echo "Stamping ticket"
backupTool stamp_ticket -h "${HW_KEY}" -s "${HDD_SERIAL}" -t "${TICKET_ENC}" -o "${fileMigrationTicket}" || error "Failed to stamp migration ticket"

echo "Ticket stamped - HC is ready for migration factory reset"
touch "${factoryResetFlag}"
exit 0
