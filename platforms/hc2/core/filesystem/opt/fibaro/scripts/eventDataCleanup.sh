#!/bin/sh
. /opt/fibaro/scripts/config
. /opt/fibaro/scripts/logging.sh

setup_self_logging "fibaro.scripts.$(basename "${0}")" "$@"

echo "Available ${eventDataDir} space: $(df -h | grep "${eventDataDir}" | awk '{print $4}')"

if [ ! -z "${logDir}" ]; then
  echo "Removing files bigger than ${maxLogFileSize} from ${logDir}..."
  find "${logDir}/" -size +"${maxLogFileSize}" -print0 | xargs -0 -r rm -v
fi

echo "Available ${eventDataDir} space after cleanup: $(df -h | grep "${eventDataDir}" | awk '{print $4}')"
