#!/bin/sh

led() {
  is_one_of() {
    echo "${2}" | grep -qw "${1}"
  }

  if is_one_of "${2}" "on,off,blink_slow,blink_fast" ; then
    if isGpioServerRunning; then ${gpioClientPath} "${1}" "${2}" > /dev/null 2>&1; else ${gpioServerPath} --setLed "${1}" --state "${2}" > /dev/null 2>&1; fi
  fi
  return $?
}

led_power() {
  led power "${1}"
  return $?
}

led_ethernet() {
  led ethernet "${1}"
  return $?
}

led_online() {
  led internet "${1}"
  return $?
}

led_recovery() {
  led recovery "${1}"
  return $?
}

led_update() {
  led update "${1}"
  return $?
}
