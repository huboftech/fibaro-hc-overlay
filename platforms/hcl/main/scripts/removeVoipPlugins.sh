#!/bin/sh

echo "Removing voip plugins"
output="${1}"

rm -rfv "${output}"/var/www/plugins/com.fibaro.carrier
rm -rfv "${output}"/var/www/plugins/com.fibaro.carrierZone
rm -rfv "${output}"/var/www/plugins/com.fibaro.dscAlarm
rm -rfv "${output}"/var/www/plugins/com.fibaro.heliosGold
rm -rfv "${output}"/var/www/plugins/com.fibaro.heliosBasic
rm -rfv "${output}"/var/www/plugins/com.fibaro.mobotix
rm -rfv "${output}"/var/www/plugins/com.fibaro.satelAlarm
rm -rfv "${output}"/var/www/plugins/com.fibaro.alphatechFarfisa
rm -rfv "${output}"/var/www/plugins/com.fibaro.baudischSip
rm -rfv "${output}"/var/www/plugins/com.fibaro.envisaLinkAlarm
rm -rfv "${output}"/var/www/plugins/com.fibaro.mockDevice

rm -rfv "${output}"/opt/fibaro/plugins/com.fibaro.carrier
rm -rfv "${output}"/opt/fibaro/plugins/com.fibaro.carrierZone
rm -rfv "${output}"/opt/fibaro/plugins/com.fibaro.dscAlarm
rm -rfv "${output}"/opt/fibaro/plugins/com.fibaro.heliosGold
rm -rfv "${output}"/opt/fibaro/plugins/com.fibaro.heliosBasic
rm -rfv "${output}"/opt/fibaro/plugins/com.fibaro.mobotix
rm -rfv "${output}"/opt/fibaro/plugins/com.fibaro.satelAlarm
rm -rfv "${output}"/opt/fibaro/plugins/com.fibaro.alphatechFarfisa
rm -rfv "${output}"/opt/fibaro/plugins/com.fibaro.baudischSip
rm -rfv "${output}"/opt/fibaro/plugins/com.fibaro.envisaLinkAlarm
rm -rfv "${output}"/opt/fibaro/plugins/com.fibaro.mockDevice

rm -rfv "${output}"/opt/fibaro/types/com.fibaro.carrier.xml
rm -rfv "${output}"/opt/fibaro/types/com.fibaro.carrierZone.xml
rm -rfv "${output}"/opt/fibaro/types/com.fibaro.dscAlarm.xml
rm -rfv "${output}"/opt/fibaro/types/com.fibaro.dscPartition.xml
rm -rfv "${output}"/opt/fibaro/types/com.fibaro.dscZone.xml
rm -rfv "${output}"/opt/fibaro/types/com.fibaro.heliosGold.xml
rm -rfv "${output}"/opt/fibaro/types/com.fibaro.heliosBasic.xml
rm -rfv "${output}"/opt/fibaro/types/com.fibaro.mobotix.xml
rm -rfv "${output}"/opt/fibaro/types/com.fibaro.satelAlarm.xml
rm -rfv "${output}"/opt/fibaro/types/com.fibaro.satelOutput.xml
rm -rfv "${output}"/opt/fibaro/types/com.fibaro.satelPartition.xml
rm -rfv "${output}"/opt/fibaro/types/com.fibaro.satelZone.xml
rm -rfv "${output}"/opt/fibaro/types/com.fibaro.envisaLinkAlarm.xml
rm -rfv "${output}"/opt/fibaro/types/com.fibaro.mockDevice.xml

rm -rfv "${output}"/etc/init.d/S90thttpd
rm -rfv "${output}"/usr/sbin/thttpd_wrapper
