#!/bin/sh

. /opt/fibaro/scripts/config

if [ ! -f "${userDataPath}/db" ]; then
  cp /opt/fibaro/db.clean "${userDataPath}/db"
  sync
fi

if [ ! -f "${userDataPath}/localtime" ]; then
  ln -s /usr/share/zoneinfo/Europe/Warsaw "${userDataPath}/localtime"
fi

if [ ! -f "${userDataPath}/icons" ]; then
  mkdir -p "${userDataPath}/icons/virtualDevices"
  mkdir -p "${userDataPath}/icons/devices"
  mkdir -p "${userDataPath}/icons/rooms"
  mkdir -p "${userDataPath}/icons/scenes"
fi

if [ ! -d "${userDataPath}/scenes" ]; then
  mkdir -p "${userDataPath}/scenes"
fi

if [ ! -d "/${userDataPath}/appData" ]; then
  mkdir -p "${userDataPath}/appData"
fi

if [ ! -d "${userDataPath}/appDataWeb" ]; then
  mkdir -p "${userDataPath}/appDataWeb"
fi

if [ ! -f "${fileNetworkInterfaces}" ]; then
  cat <<EndCfg > "${fileNetworkInterfaces}" 
  auto lo
  iface lo inet loopback

  auto eth0
  iface eth0 inet dhcp
EndCfg
fi

# check system_data/nvm
if [ ! -d "${nvmDataDir}" ]; then
  mkdir -p "${nvmDataDir}"
fi

if [ ! -f "${currentZwaveVersion}" ]; then
  # current version is unknown, so an empty file is created
  # next zwave flash version check will always result in update
  # (new version would be saved in this file)
  touch "${currentZwaveVersion}"
fi
