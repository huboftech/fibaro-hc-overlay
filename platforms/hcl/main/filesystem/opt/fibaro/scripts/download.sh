#!/bin/sh
. /opt/fibaro/scripts/config
. /opt/fibaro/system/status-utils
. /opt/fibaro/scripts/utils/errorCodeMessages.sh

for_each_line()
{
    local cmd=${1}
    while read -r line; do
        ${cmd} "${line}"
    #done < /dev/stdin
    done < /proc/self/fd/0 # HCL does not have /dev/stdin (lack of coreutils package)
                           # applied temporary change to make patch downloading work
                           # this shall be fixed in HO-348 (statuses)
}

download()
{
    # [URI] [OUT_FILE] [PROGRESS_ENABLED]
    local uri=${1}
    local out="${2}"
    local send_progress="${3}"
    # those two are optional and depends on send_progress
    local update_type="${4}"
    local update_version="${5}"

    http_status() {
    	# -I (head instead of get)
        # shellcheck disable=SC2086
        curl -s -o /dev/null -I -w "%{http_code}" ${1}
    }

    local http_code; http_code=$(http_status "${uri}")
    local curl_code=$?

    if [ "${http_code}" -eq 0 ] && [ "${curl_code}" -ne 0 ]; then
        errorCodeType="curl"
        errorCode=${curl_code}
        return 1
    elif [ "${http_code}" -lt 200 ] || [ "${http_code}" -ge 300 ]; then
        errorCodeType="http"
        errorCode=${http_code}
        return 1
    fi

    split_curl_progress() {
        stdbuf -oL tr '\r' '\n' | stdbuf -oL sed -r 's/[# ]+//g'
    }

    send_download_progress() {
        if [ "${send_progress}" = "1" ] && [ ! -z "${1}" ]; then
          if echo "${1}" | grep -i -E '^([0-9]{1,2}[.][0-9][%])$' >/dev/null 2>&1 ; then
            set_status_downloading_progress "${1}" "${update_type}" "${update_version}"
          fi
        fi
    }
    # shellcheck disable=SC2086
    curl ${uri} -o "${out}" --connect-timeout "${downloadTimeout}" --retry 3 --progress-bar 2>&1 | \
        split_curl_progress | for_each_line "send_download_progress"
}


# main
download "$@" || {
  _error_code_msg "${errorCodeType}" "${errorCode}"
  exit 1
}

exit 0
