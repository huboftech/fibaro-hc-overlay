require('json')
require('net.HTTPClient')

function handleResponse(response)
  responseJson = json.decode(response.data)
  print(json.encode(responseJson))

  local autoSSHCommand = 'ssh -y -K 30 -i /etc/dropbear/dropbear_rsa_host_key -R '  .. responseJson.private_ip..  ':' .. responseJson.port .. ':localhost:22 remote2@' .. responseJson.ip
  os.execute(autoSSHCommand)
end

function getSupportData()
  remoteUrl='https://dom.fibaro.com/get_support_route.php?PK_AccessPoint=' .. serialNumber .. '&HW_Key=' .. HWKey
  print(remoteUrl)

  http = net.HTTPClient({timeout = 5000})

  http:request(remoteUrl, {
    options = {
      method = 'GET'
    },
    success = function(response)
      handleResponse(response)
    end,
    error = function(error)
      print(error)
    end
  })
end

getSupportData()
