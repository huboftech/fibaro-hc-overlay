#!/bin/sh

tmpBackupPath="/tmp/backups"
backupPath="/mnt/download/backups"
backupFilename="backup.7z"

remoteBackupServer="https://backups.fibaro.com"
remoteGetBackupsListScript="${remoteBackupServer}/getBackupsList.php"
remoteSaveBackupScript="${remoteBackupServer}/saveBackup.php"
remoteGetBackupScript="${remoteBackupServer}/getBackup.php"
remoteDeleteBackupScript="${remoteBackupServer}/deleteBackup.php"
remoteGetBackupKeyScript="${remoteBackupServer}/getBackupsKey.php"

restoreUserData() {
  safe_clean_dir_except "${userDataPath}" "${nvmData}" || return 1
  safe_move_dir_content_except "${tmpBackupPath}" "${userDataPath}" "${nvmData}" || return 2
  safe_remove_dir "${tmpBackupPath}" || return 3
}

_startServicesIfManualBackup()
{
   ${modules} start

	if [ "${autoBackup}" != "true" ]; then
		set_status_starting_services
		${fibaroStart} start
	fi
}

_createBackupFile()
{
    ### zwave dump ###
    echo "Backing up Z-Wave..."
    set_status_backup_create_zwave_dump

    if ! readZwaveEeprom "${userDataPath}/${zwaveFile}" ; then
        set_status_backup_create_error_while_dumping_zwave
        _endScript 2
    fi

    echo "Backing up user data..."
    set_status_backup_create_user_data
    cd "${userDataPath}" || { set_status_backup_create_error_while_backing_up_user_data; _endScript 3; }
    rm -rf "${tmpBackupPath}"
    # shellcheck disable=SC2086
    tar -cf - ./* | 7zr a -m0=lzma2 -md=1M "${tmpBackupPath}/${backupFilename}" -xr!${nvmData} || { set_status_backup_create_error_while_backing_up_user_data; _endScript 4; }

    echo "Backup file size: $(du -h "${tmpBackupPath}/${backupFilename}" | awk '{print $1}')"
}

_encryptBackup()
{
  echo "HCL doesn't support backups encryption"
  # NOP
}

_decryptBackup()
{
  _encryptBackup
}

_deletePendriveBackup()
{
  # HCL doesn't support pendrive backups
  :
}
