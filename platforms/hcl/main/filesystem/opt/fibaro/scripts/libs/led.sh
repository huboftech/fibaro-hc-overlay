#!/bin/sh

led() {
  is_one_of() {
    echo "${2}" | grep -qw "${1}"
  }

  if is_one_of "${2}" "on,off,blink_slow,blink_fast" ; then
    if isModulesRunning ; then
      ${modulesClientPath} led "${1}" "${2}" &>/dev/null
    else
      ledNumber=0

      get_led_number()
      {
        led_map_put() {
          # shellcheck disable=SC2140
          eval led_map_"$1"="$2"
        }

        led_map_get() {
          # waiting for brave to fix it
          # shellcheck disable=SC2016
          eval echo '${led_map_'"$1"'}'
        }
        led_map_put update 1
        led_map_put zwave 2
        led_map_put internet 3
        led_map_put ethernet 4
        led_map_put power 5

        # shellcheck disable=SC2005
        # shellcheck disable=SC2046
        echo $(led_map_get "${1}")
      }

      # shellcheck disable=SC2046
      ${modulesPath} --"${2}"=$(get_led_number "${1}") &>/dev/null
    fi
  fi
  return $?
}

led_power() {
  led power "$1"
  return $?
}

led_ethernet() {
  led ethernet "$1"
  return $?
}

led_online() {
  led internet "$1"
  return $?
}

led_update() {
  led update "$1"
  return $?
}

led_zwave() {
  led zwave "$1"
  return $?
}

enable_zwave_led() {
  if isModulesRunning ; then
    ${modulesClientPath} zwave enable led
  else
    ${modulesPath} --ledMode 1
  fi
  return $?
}
