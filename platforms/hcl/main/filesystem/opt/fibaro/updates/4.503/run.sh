#!/bin/sh

. /opt/fibaro/scripts/config

DIR=$(getFullDirName "${0}")

sqlExecByDir "${DIR}"/sql

prepareNetworkConf() {
    local networkConf; networkConf="$(/opt/fibaro/scripts/net.sh get)"
    local networkConfFile="/mnt/user_data/network.conf"
    local dhcp; dhcp="$(echo "${networkConf}" | cut -d '|' -f 1)"

    if [ "${dhcp}" = "1" ] ; then
        echo "dhcp=1" > "${networkConfFile}"
    else
        local address; address="$(echo "${networkConf}" | cut -d '|' -f 2)"
        local netmask; netmask="$(echo "${networkConf}" | cut -d '|' -f 3)"
        local gateway; gateway="$(echo "${networkConf}" | cut -d '|' -f 4)"
        local dns; dns="$(echo "${networkConf}" | cut -d '|' -f 5)"

        { echo "dhcp=0";
          echo "address=${address}";
          echo "netmask=${netmask}";
          echo "gateway=${gateway}";
          echo "dns=${dns}"; } > "${networkConfFile}"
    fi

    sync
}

prepareNetworkConf