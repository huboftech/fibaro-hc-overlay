
insert or replace into LogTranslation values ("pl", "TXT_COMMON_NEW_ADDED_SCENE", "Nowa scena");
insert or replace into LogTranslation values ("en", "TXT_COMMON_NEW_ADDED_SCENE", "New Scene");

insert or replace into LogTranslation values ("pl", "TXT_EMAIL_ALERT_POPUP_DESC", "Twój adres e-mail jest pusty. Zalecamy wypełnienie tego pola. Jeśli tego nie zrobisz nie będziesz mógł przywrócić swojego hasła.<br><br>To powiadomienie zniknie dopiero po jego uzupełnieniu.");
insert or replace into LogTranslation values ("en", "TXT_EMAIL_ALERT_POPUP_DESC", "Your e-mail address field is empty. It is strongly recommended to add e-mail in order to be able to reset your password.<br><br>This notification will be discarded after you add e-mail.");

insert or replace into LogTranslation values ("pl", "TXT_EMAIL_ALERT_POPUP_LINK_USERS_PANEL", "Panel uprawnień");
insert or replace into LogTranslation values ("en", "TXT_EMAIL_ALERT_POPUP_LINK_USERS_PANEL", "Access control panel");

insert or replace into LogTranslation values ("pl", "TXT_FIBAROALARM_SATE_ALERT", "Uwaga, integracja z centralą alarmową jest nie w pełni skonfigurowana. Centrala nie posiada strefy alarmowej o nazwie Fibaro.<br><br>To powiadomienie zniknie dopiero po pełnej konfiguracji.");
insert or replace into LogTranslation values ("en", "TXT_FIBAROALARM_SATE_ALERT", "Attention, integration with the alarm control panel is not fully configured. The control panel does not have a alarm partition named Fibaro.<br><br>This notification will be discarded after full configuration.");

insert or replace into LogTranslation values ("pl", "TXT_ZWAVE_POLLING_TIME_ALERT_POPUP_DESC", "Zalecamy zwiększyć interwał odpytywania urządzeń.<br><br>To powiadomienie zniknie dopiero po jego zwiększeniu.");
insert or replace into LogTranslation values ("en", "TXT_ZWAVE_POLLING_TIME_ALERT_POPUP_DESC", "It is highly recommended to increase devices polling time interval.<br><br>This notification will be discarded after you increase devices polling time interval.");

insert or replace into LogTranslation values ("pl", "TXT_ZWAVE_POLLING_TIME_ALERT_POPUP_LINK", "Konfiguracji sieci Z-Wave");
insert or replace into LogTranslation values ("en", "TXT_ZWAVE_POLLING_TIME_ALERT_POPUP_LINK", "Z-Wave network configuration");

insert or replace into LogTranslation values ("pl", "TXT_ZWAVE_NUMBER_OF_DEVICES", "Liczba urządzeń");
insert or replace into LogTranslation values ("en", "TXT_ZWAVE_NUMBER_OF_DEVICES", "Number of devices");

insert or replace into LogTranslation values ("pl", "TXT_ZWAVE_POLLING_TIME_RECOMMENDATION", "Zalecany minimalny interwał odpytywania urządzeń to");
insert or replace into LogTranslation values ("en", "TXT_ZWAVE_POLLING_TIME_RECOMMENDATION", "Recommended devices polling time interval:");

insert or replace into LogTranslation values ("pl", "TXT_UNCONFIGURED_ALERT_POPUP_LINK_ZWAVE_PANEL", "Panel diagnostyki Z-Wave");
insert or replace into LogTranslation values ("en", "TXT_UNCONFIGURED_ALERT_POPUP_LINK_ZWAVE_PANEL", "Z-Wave diagnostic panel");

insert or replace into LogTranslation values ("pl", "TXT_UNCONFIGURED_ALERT_POPUP_DESC", "Nie wszystkie urządzenia dodane do sieci Z-Wave są skonfigurowane prawidłowo. Zaleca się ich rekonfigurację w celu zapewnienia pełnej funkcjonalności systemu.<br><br>To powiadomienie zniknie dopiero po skonfigurowaniu wszystkich urządzeń.");
insert or replace into LogTranslation values ("en", "TXT_UNCONFIGURED_ALERT_POPUP_DESC", "Not all devices added to the Z-Wave network are configured correctly. It is recommended to do the reconfiguration to ensure full functionality of the system.<br><br>This notification will be discarded when all devices will be configured correctly.");

insert or replace into LogTranslation values ("pl", "TXT_ZWAVE_POLLING_TIME_ALERT_POPUP_TITLE", "Nieprawidłowy interwał odpytywania urządzeń");
insert or replace into LogTranslation values ("en", "TXT_ZWAVE_POLLING_TIME_ALERT_POPUP_TITLE", "Invalid polling time interval");

insert or replace into LogTranslation values ("pl", "TXT_EMAIL_ALERT_POPUP_TITLE", "Niewypełniony adres email");
insert or replace into LogTranslation values ("en", "TXT_EMAIL_ALERT_POPUP_TITLE", "E-mail address field is empty");

insert or replace into LogTranslation values ("pl", "TXT_UNCONFIGURED_ALERT_POPUP_TITLE", "Nieskonfigurowane urządzenia");
insert or replace into LogTranslation values ("en", "TXT_UNCONFIGURED_ALERT_POPUP_TITLE", "Unconfigured devices");

insert or replace into LogTranslation values ("pl", "TXT_WITHOUTTEMPLATE_ALERT_POPUP_TITLE", "Urządzenia bez szablonów");
insert or replace into LogTranslation values ("en", "TXT_WITHOUTTEMPLATE_ALERT_POPUP_TITLE", "Devices without templates");

insert or replace into LogTranslation values ("pl", "TXT_FIBAROALARM_SATE_ALERT_TITLE", "Niekompletna konfiguracja centrali alarmowej");
insert or replace into LogTranslation values ("en", "TXT_FIBAROALARM_SATE_ALERT_TITLE", "Not fully configured alarm control panel");

insert or replace into LogTranslation values ("pl", "TXT_WITHOUTTEMPLATE_ALERT_POPUP_DESC_NEW", "Niektóre urządzenia w Twojej sieci Z-Wave nie posiadają szablonu Z-Wave.<br />Jeśli nie działają prawidłowo lub brakuje im jakichś funkcjonalności, pobierz szablon urządzenia i wyślij go na support@fibaro.com.<br />Przygotujemy odpowiedni szablon Z-Wave aby rozwiązać problemy i zapewnić pełne wsparcie urządzenia.<br />Brak szablonu parametrów nie oznacza, że urządzenie jest niewspierane. W dalszym ciągu możesz dodać parametry konfiguracyjne zgodnie z instrukcją obsługi, korzystając z zakładki Konfiguracji Zaawansowanej urządzenia.<br />Ta notyfikacja zniknie gdy wszystkie urządzenia będą miały szablony Z-Wave.");
insert or replace into LogTranslation values ("en", "TXT_WITHOUTTEMPLATE_ALERT_POPUP_DESC_NEW", "Some devices in your Z-Wave network do not have Z-Wave templates.<br />If they do not work properly or miss some functionalities, download the device's template and send it to support@fibaro.com.<br />We will prepare the appropriate Z-Wave template to fix the problem and provide full support of the device.<br />The lack of parameters template does not mean that device is not supported. You can still add configuration parameters manually according to device's manual using the Advanced Configuration page.<br />This notification will disappear when all devices will have Z-Wave templates.");
