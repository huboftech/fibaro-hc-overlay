#!/bin/sh

. /opt/fibaro/scripts/config

DIR=$(getFullDirName "${0}")

sqlExecByDir "${DIR}"/sql

ntpServer="$(sqlite3 /mnt/user_data/db 'SELECT NTP_Server FROM HC_Location;')"
echo "server $ntpServer iburst" > /mnt/user_data/ntp.conf
