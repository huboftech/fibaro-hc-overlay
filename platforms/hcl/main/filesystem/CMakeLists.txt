cmake_minimum_required(VERSION 2.8)

message("Adding HCL main system filesystem")

install(DIRECTORY etc opt usr var DESTINATION ${CMAKE_INSTALL_PREFIX} USE_SOURCE_PERMISSIONS)
