#!/bin/sh

FILE_FIBARO_PUBLIC_KEY="/mnt/hw_data/fibaro.pub"
WORKING_DIR="/tmp/tester"
URL_DIR="http://production-server.fibaro.com/hclite/tester"

productionTest=$(lsusb | grep -c "ffff:ffff")

if [ "${productionTest}" -eq 1 ]; then
  rm -rf "$WORKING_DIR"
  mkdir -p "$WORKING_DIR"
  cd "$WORKING_DIR" || exit 1

  wget $URL_DIR"/package.tar.gz" || exit 1
  wget $URL_DIR"/package.signature" || exit 2

  openssl dgst -sha256 < package.tar.gz | awk -F"= " '{ print $2 }' > hash
  openssl rsautl -verify -inkey $FILE_FIBARO_PUBLIC_KEY -keyform PEM -pubin -in package.signature | awk -F"= " '{ print $2 }' > verified

  
  if diff hash verified; then
    gunzip -c package.tar.gz | tar -xf -
    ./run.sh
  fi

  cd / || exit 2
  rm -rf "$WORKING_DIR"
fi
