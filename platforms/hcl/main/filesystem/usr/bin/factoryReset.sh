#!/bin/sh

. /opt/fibaro/scripts/config
. /opt/fibaro/scripts/logging.sh

setup_self_logging "fibaro.scripts.$(basename "${0}")" "$@"

echo "Triggering factory reset - no migration ticket for HCL"
touch "${factoryResetFlag}"
exit 0
